/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2013, Willow Garage, Inc.
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of Willow Garage, Inc. nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Sachin Chitta */


#include <moveit/workspace_analysis/workspace_analysis.h>
#include <eigen_conversions/eigen_msg.h>
#include <fstream>
#include <iostream>

#include <cmath>

#include <ros/package.h>
#include <string>
#include <sstream>

#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

//#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/robot_state/attached_body.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

//#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <shape_msgs/SolidPrimitive.h>
#include <geometric_shapes/shapes.h>

// Kinematics
#include <moveit_msgs/GetPositionIK.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#define NUM_QUATERNIONS 101 //101.0

//funtion below taken from: https://github.com/warehouse-picking-automation-challenges/team_delft/blob/master/apc16delft_motion/src/trajectory_generator.cpp
namespace {
  void storeYamlPlan(std::string from, std::string to, moveit::planning_interface::MoveGroup::Plan plan)
  {
    std::string path = ros::package::getPath("moveit_workspace_analysis");

    trajectory_msgs::JointTrajectory trajectory = plan.trajectory_.joint_trajectory;

    std::ofstream os;
    std::stringstream ss;
    ss << path << "/trajectories/" << from << "_to_" << to << ".yaml";
    ROS_DEBUG("Storing YAML: %s", ss.str().c_str());

    os.open(ss.str());
    os << "joint_trajectory: \n  header: \n    seq: 0\n    stamp: \n      secs: 0\n      nsecs: 0\n    frame_id: /world\n";
    os << "  joint_names: ['";

    std::vector<std::string> jns = trajectory.joint_names;
    for (int ii = 0; ii < jns.size() - 1; ii++)
      os << jns[ii] << "', '";
    os << jns.back() << "']\n";

    os << "  points: \n";

    for (int i = 0; i < trajectory.points.size(); i++)
    {
      trajectory_msgs::JointTrajectoryPoint jtp = trajectory.points[i];

      os << "    - \n";
      os << "      positions: [";
      for (int ii = 0; ii < jtp.positions.size() - 1; ii++)
        os << jtp.positions[ii] << ", ";
      os << jtp.positions.back() << "]\n";

      os << "      velocities: [";
      for (int ii = 0; ii < jtp.velocities.size() - 1; ii++)
        os << jtp.velocities[ii] << ", ";
      os << jtp.velocities.back() << "]\n";

      os << "      accelerations: [";
      for (int ii = 0; ii < jtp.accelerations.size() - 1; ii++)
        os << jtp.accelerations[ii] << ", ";
      os << jtp.accelerations.back() << "]\n";

      os << "      effort: []\n";
      os << "      time_from_start: \n";
      os << "        secs: " << jtp.time_from_start.sec << "\n";
      os << "        nsecs: " << jtp.time_from_start.nsec << "\n";
    }
    os.close();
  }
}

namespace moveit_workspace_analysis
{

WorkspaceAnalysis::WorkspaceAnalysis(const planning_scene::PlanningSceneConstPtr &planning_scene,
                                     bool position_only,
                                     double joint_limits_penalty_multiplier)
    : planning_scene_(planning_scene), position_only_ik_(position_only), canceled_(false)
{
  state_validity_callback_fn_ = boost::bind(&WorkspaceAnalysis::isIKSolutionCollisionFree, this, _1, _2, _3);
  kinematics_metrics_.reset(new kinematics_metrics::KinematicsMetrics(planning_scene->getCurrentState().getRobotModel()));
  kinematics_metrics_->setPenaltyMultiplier(joint_limits_penalty_multiplier);
}

bool WorkspaceAnalysis::isIKSolutionCollisionFree(robot_state::RobotState *joint_state,
                                                  const robot_model::JointModelGroup *joint_model_group,
                                                  const double *ik_solution)
{
  joint_state->setJointGroupPositions(joint_model_group, ik_solution);
  std::string joint_model_group_name = joint_model_group->getName();
  bool result = !planning_scene_->isStateColliding(*joint_state, joint_model_group_name);
  return result;
}

std::vector<geometry_msgs::Pose> WorkspaceAnalysis::sampleUniform(const moveit_msgs::WorkspaceParameters &workspace,
                                                                  const std::vector<geometry_msgs::Quaternion> &orientations,
                                                                  double x_resolution,
                                                                  double y_resolution,
                                                                  double z_resolution) const
{
  std::vector<geometry_msgs::Pose> results;
  std::vector<geometry_msgs::Quaternion> rotations = orientations;
  if (orientations.empty())
  {
    geometry_msgs::Quaternion quaternion;
    quaternion.w = 1.0;
    rotations.push_back(quaternion);
  }
  double x_min = workspace.min_corner.x;
  double y_min = workspace.min_corner.y;
  double z_min = workspace.min_corner.z;

  unsigned int x_num_points, y_num_points, z_num_points;
  double x_dim = std::fabs(workspace.min_corner.x - workspace.max_corner.x);
  double y_dim = std::fabs(workspace.min_corner.y - workspace.max_corner.y);
  double z_dim = std::fabs(workspace.min_corner.z - workspace.max_corner.z);

  x_num_points = (unsigned int)(x_dim / x_resolution) + 1;
  y_num_points = (unsigned int)(y_dim / y_resolution) + 1;
  z_num_points = (unsigned int)(z_dim / z_resolution) + 1;

  ROS_DEBUG("Cache dimension (num grid points) in (x,y,z): %d %d %d", x_num_points, y_num_points, z_num_points);
  geometry_msgs::Pose pose;
  for (std::size_t i = 0; i < x_num_points; ++i)
  {
    pose.position.x = x_min + i * x_resolution;
    for (std::size_t j = 0; j < y_num_points; ++j)
    {
      pose.position.y = y_min + j * y_resolution;
      for (std::size_t k = 0; k < z_num_points; ++k)
      {
        pose.position.z = z_min + k * z_resolution;
        for (std::size_t m = 0; m < rotations.size(); ++m)
        {
          pose.orientation = rotations[m];
          results.push_back(pose);
        }
      }
    }
  }
  ROS_INFO("Generated %d samples for workspace points", (int)results.size());
  return results;
}

std::vector<geometry_msgs::Pose> WorkspaceAnalysis::sampleUniformForHumanArm(const moveit_msgs::WorkspaceParameters &workspace,
                                                                             double x_resolution, 
                                                                             double y_resolution, 
                                                                             double z_resolution, 
                                                                             double rpy_resolution,
                                                                             double mean,
                                                                             double stddev) const
{
  
  std::default_random_engine generator;
  std::normal_distribution<double> dist(mean, stddev);

  std::vector<geometry_msgs::Pose> results;

  double x_min = workspace.min_corner.x;
  double y_min = workspace.min_corner.y;
  double z_min = workspace.min_corner.z;

  unsigned int x_num_points, y_num_points, z_num_points;
  double x_dim = std::fabs(workspace.min_corner.x - workspace.max_corner.x);
  double y_dim = std::fabs(workspace.min_corner.y - workspace.max_corner.y);
  double z_dim = std::fabs(workspace.min_corner.z - workspace.max_corner.z);

  x_num_points = (unsigned int)(x_dim / x_resolution) + 1;
  y_num_points = (unsigned int)(y_dim / y_resolution) + 1;
  z_num_points = (unsigned int)(z_dim / z_resolution) + 1;

  ROS_INFO("Cache dimension (num grid points) in (x,y,z): %d %d %d", x_num_points, y_num_points, z_num_points);
  geometry_msgs::Pose pose;
  
  std::vector<double> values;
  for (double value = -M_PI; value < M_PI; value += rpy_resolution)
    values.push_back(value);

  std::vector<double> rolls = values;
  std::vector<double> pitchs = values;
  std::vector<double> yaws = values;

  ROS_INFO("Cache rotation dimensions (num grid points) in (r,p,y): %d %d %d", rolls.size(), pitchs.size(), yaws.size());

  for (std::size_t i = 0; i < x_num_points; ++i)
  {
    pose.position.x = x_min + i * x_resolution + dist(generator);
    for (std::size_t j = 0; j < y_num_points; ++j)
    {
      pose.position.y = y_min + j * y_resolution + dist(generator);
      for (std::size_t k = 0; k < z_num_points; ++k)
      {
        pose.position.z = z_min + k * z_resolution + dist(generator);
        for (std::size_t m = 0; m < rolls.size(); ++m)
        {
            for (std::size_t n = 0; n < pitchs.size(); ++n)
            {
                for (std::size_t p = 0; p < yaws.size(); ++p)
                {
                    tf2::Quaternion quat;
                    quat.setRPY(rolls[m], pitchs[n], yaws[p]);
                    quat.normalize();
                    tf2::convert(quat, pose.orientation);
                    results.push_back(pose);
                }
            }
        }
      }
    }
  }
  ROS_INFO("Generated %d samples for workspace points", (int)results.size());
  //for (int i = 0; i < results.size(); i++) {
  //  ROS_INFO_STREAM(results[i]);
  //}
  //sleep(10);
  return results;
}

WorkspaceMetrics WorkspaceAnalysis::computeMetrics(const moveit_msgs::WorkspaceParameters &robot_workspace,
                                                   const std::vector<geometry_msgs::Quaternion> &robot_orientations,
                                                   const moveit_msgs::WorkspaceParameters &human_right_arm_workspace,
                                                   const moveit_msgs::WorkspaceParameters &human_left_arm_workspace,
                                                   robot_state::RobotState *joint_state,
                                                   const robot_model::JointModelGroup *joint_model_group,
                                                   double x_resolution,
                                                   double y_resolution,
                                                   double z_resolution,
                                                   double rpy_resolution) const
{
  WorkspaceMetrics metrics;
  if (!joint_model_group || !planning_scene_)
  {
    ROS_ERROR("Joint state group and planning scene should not be null");
    return metrics;
  }
  std::vector<geometry_msgs::Pose> points = sampleUniform(robot_workspace, robot_orientations, x_resolution, y_resolution, z_resolution);
  /*
    - write sampleUniform to a file
  */
  metrics.group_name_ = joint_model_group->getName();
  metrics.robot_name_ = joint_state->getRobotModel()->getName();
  metrics.frame_id_ = joint_state->getRobotModel()->getModelFrame();

  ROS_INFO_STREAM("Root frame ID: " << metrics.frame_id_);

  //HUMAN
  ROS_INFO_NAMED("Planning", "instantiating planning group for human left arm");
  moveit::planning_interface::MoveGroupInterface left_arm_move_group("human_left_arm");
  
  ROS_INFO_NAMED("Planning", "instantiated planning group for human right arm");
  moveit::planning_interface::MoveGroupInterface right_arm_move_group("human_right_arm");
  ROS_INFO_NAMED("Planning", "instantiated planning group for human right arm");
  
  static const std::string PLANNING_GROUP = "panda_arm";
  ROS_INFO_NAMED("Planning", "instantiating planning group");
  moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);

  move_group.setEndEffectorLink("panda_gripper_center");
  ROS_INFO_STREAM("MoveGroup EE: " << move_group.getEndEffectorLink());

  std::vector<double> home_position = { 0, -0.785, 0, -2.356, 0, 1.571, 0.785 };
  move_group.setJointValueTarget(home_position);
  move_group.move();

  //ROS_INFO_NAMED("Planning", "instantiated planning group");
  //moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  
  // Visualization
  // ^^^^^^^^^^^^^
  //
  // The package MoveItVisualTools provides many capabilties for visualizing objects, robots,
  // and trajectories in RViz as well as debugging tools such as step-by-step introspection of a script
  //namespace rvt = rviz_visual_tools;
  //moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
  //visual_tools.deleteAllMarkers();

  //use sample uniform to get a uniform sampling of cylinder arms but with lower resolution
  std::vector<geometry_msgs::Pose> arm_points_1_raw = sampleUniformForHumanArm(human_right_arm_workspace, x_resolution, y_resolution, z_resolution, rpy_resolution, 0.0, 0.05);

  //use sample uniform to get a uniform sampling of cylinder arms but with lower resolution
  std::vector<geometry_msgs::Pose> arm_points_2_raw = sampleUniformForHumanArm(human_left_arm_workspace, x_resolution, y_resolution, z_resolution, rpy_resolution, 0.0, 0.05);
  
  std::vector<geometry_msgs::Pose> arm_points_1;
  std::vector<geometry_msgs::Pose> arm_points_2;
  
  ros::NodeHandle node_handle;

  for (std::size_t i = 0; i < arm_points_1_raw.size(); ++i){

    // Start a service client
    ros::ServiceClient service_client = node_handle.serviceClient<moveit_msgs::GetPositionIK> ("compute_ik");

    while(!service_client.exists()){
        ROS_INFO("Waiting for service");
        sleep(1.0);
    }

    moveit_msgs::GetPositionIK::Request service_request;
    moveit_msgs::GetPositionIK::Response service_response;
    
    service_request.ik_request.group_name = "human_right_arm";
    service_request.ik_request.pose_stamped.header.frame_id = "world";  
    service_request.ik_request.pose_stamped.pose.position.x = arm_points_1_raw[i].position.x;
    service_request.ik_request.pose_stamped.pose.position.y = arm_points_1_raw[i].position.y;
    service_request.ik_request.pose_stamped.pose.position.z = arm_points_1_raw[i].position.z;
    
    service_request.ik_request.pose_stamped.pose.orientation.x = arm_points_1_raw[i].orientation.x;
    service_request.ik_request.pose_stamped.pose.orientation.y = arm_points_1_raw[i].orientation.y;
    service_request.ik_request.pose_stamped.pose.orientation.z = arm_points_1_raw[i].orientation.z;
    service_request.ik_request.pose_stamped.pose.orientation.w = arm_points_1_raw[i].orientation.w;

    /* Call the service */
    service_client.call(service_request, service_response);
    if((service_response.error_code.val == service_response.error_code.SUCCESS)){
        arm_points_1.push_back(arm_points_1_raw[i]);
    }
    //ROS_INFO_STREAM("Result: " << ((service_response.error_code.val == service_response.error_code.SUCCESS) ? "True " : "False ") << service_response.error_code.val);
  }

  for (std::size_t i = 0; i < arm_points_2_raw.size(); ++i){
    

    // Start a service client
    ros::ServiceClient service_client = node_handle.serviceClient<moveit_msgs::GetPositionIK> ("compute_ik");

    while(!service_client.exists()){
        ROS_INFO("Waiting for service");
        sleep(1.0);
    }

    moveit_msgs::GetPositionIK::Request service_request;
    moveit_msgs::GetPositionIK::Response service_response;
    
    service_request.ik_request.group_name = "human_left_arm";
    service_request.ik_request.pose_stamped.header.frame_id = "world";  
    service_request.ik_request.pose_stamped.pose.position.x = arm_points_2_raw[i].position.x;
    service_request.ik_request.pose_stamped.pose.position.y = arm_points_2_raw[i].position.y;
    service_request.ik_request.pose_stamped.pose.position.z = arm_points_2_raw[i].position.z;
    
    service_request.ik_request.pose_stamped.pose.orientation.x = arm_points_2_raw[i].orientation.x;
    service_request.ik_request.pose_stamped.pose.orientation.y = arm_points_2_raw[i].orientation.y;
    service_request.ik_request.pose_stamped.pose.orientation.z = arm_points_2_raw[i].orientation.z;
    service_request.ik_request.pose_stamped.pose.orientation.w = arm_points_2_raw[i].orientation.w;

    /* Call the service */
    service_client.call(service_request, service_response);
    if((service_response.error_code.val == service_response.error_code.SUCCESS)){
        arm_points_2.push_back(arm_points_2_raw[i]);
    }
    //ROS_INFO_STREAM("Result: " << ((service_response.error_code.val == service_response.error_code.SUCCESS) ? "True " : "False ") << service_response.error_code.val);
  }
  
  ROS_INFO("arm_points_1 size %d", (int)arm_points_1.size());
  ROS_INFO("arm_points_2 size %d", (int)arm_points_2.size());
  
  int ik_good = 0, ik_bad = 0;
  int last_ik_good = 0, last_ik_bad = 0;
  ros::Time start = ros::Time::now();
  for (std::size_t i = 0; i < points.size(); ++i)
  {
    if (!ros::ok() || canceled_)
      return metrics;
    bool found_ik = joint_state->setFromIK(joint_model_group, points[i], 5, 0.01, state_validity_callback_fn_); //unsigned int attempts=5, double timeout=0.005
    int plan_counter = 0;
    if (found_ik)
    {
      ik_good++;
      last_ik_good++;
      metrics.points_.push_back(points[i]);
      updateMetrics(joint_state, joint_model_group, metrics);

      // Raw pointers are frequently used to refer to the planning group for improved performance.
      //const robot_state::JointModelGroup* joint_model_group = move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

        for (std::size_t k = 0; k < arm_points_1.size() || k < arm_points_2.size(); ++k){

            
            geometry_msgs::PoseStamped cylinder_pose_1;
            cylinder_pose_1.header.frame_id="/world";
            //cylinder_pose_1.header.stamp = ros::Time::now();
            cylinder_pose_1.pose.orientation.w = arm_points_1[k].orientation.w;
            cylinder_pose_1.pose.orientation.x = arm_points_1[k].orientation.x;
            cylinder_pose_1.pose.orientation.y = arm_points_1[k].orientation.y;
            cylinder_pose_1.pose.orientation.z = arm_points_1[k].orientation.z;
            cylinder_pose_1.pose.position.x = arm_points_1[k].position.x;
            cylinder_pose_1.pose.position.y = arm_points_1[k].position.y;
            cylinder_pose_1.pose.position.z = arm_points_1[k].position.z;

            geometry_msgs::PoseStamped cylinder_pose_2;
            cylinder_pose_2.header.frame_id="/world";
            //cylinder_pose_2.header.stamp = ros::Time::now();
            cylinder_pose_2.pose.orientation.w = arm_points_2[k].orientation.w;
            cylinder_pose_2.pose.orientation.x = arm_points_2[k].orientation.x;
            cylinder_pose_2.pose.orientation.y = arm_points_2[k].orientation.y;
            cylinder_pose_2.pose.orientation.z = arm_points_2[k].orientation.z;
            cylinder_pose_2.pose.position.x = arm_points_2[k].position.x;
            cylinder_pose_2.pose.position.y = arm_points_2[k].position.y;
            cylinder_pose_2.pose.position.z = arm_points_2[k].position.z;
            

            //move human arms into position
            right_arm_move_group.setPoseTarget(cylinder_pose_1);
            moveit::planning_interface::MoveGroupInterface::Plan right_arm_plan;
            bool right_success = (right_arm_move_group.plan(right_arm_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

            if (right_success)
                right_arm_move_group.move();

            left_arm_move_group.setPoseTarget(cylinder_pose_2);
            moveit::planning_interface::MoveGroupInterface::Plan left_arm_plan;
            bool left_success = (left_arm_move_group.plan(left_arm_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

            if (left_success)
                left_arm_move_group.move();

            if (right_success || left_success){
                //visual_tools.publishTrajectoryLine(my_plan.trajectory_, joint_model_group);
                //visual_tools.trigger();
                std::string pos_x = std::to_string(points[i].position.x);
                std::string pos_y = std::to_string(points[i].position.y);
                std::string pos_z = std::to_string(points[i].position.z);
                std::string qx = std::to_string(points[i].orientation.x);
                std::string qy = std::to_string(points[i].orientation.y);
                std::string qz = std::to_string(points[i].orientation.z);
                std::string qw = std::to_string(points[i].orientation.w);
                
                //std::string counter_k = std::to_string(k);

                //ROS_INFO_NAMED("Planning", points[i]);

                move_group.setPoseTarget(points[i]);

                moveit::planning_interface::MoveGroupInterface::Plan my_plan;

                bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);

                if (success){
                    //ROS_INFO_NAMED("Planning", "Plan: %s", success ? "" : "FAILED");
                    std::string to = pos_x + "_" + pos_y + "_" + pos_z + "_" + qx + "_" + qy + "_" + qz + "_" + qw + "plan" + "_" + std::to_string(plan_counter);
                    storeYamlPlan("home", to, my_plan);
                    plan_counter++;
                }
            }
            /*else{
                ROS_INFO_NAMED("Planning", "No plans found!");
            }*/

            //visual_tools.deleteAllMarkers();

        }
    }
    else
    {
      ik_bad++;
      last_ik_bad++;
    }
    if ((i + 1) % (int)NUM_QUATERNIONS == 0)
    {
      double elapsed_time = (ros::Time::now() - start).toSec();
      ROS_INFO_STREAM(ik_good << " IK found and " << ik_bad << " NOT found (" << (double)ik_good / (i + 1) * 100.0 << "%), LAST pose only " << last_ik_good << " IK found and " << last_ik_bad << " NOT found (" << (double)last_ik_good / NUM_QUATERNIONS * 100.0 << "%); estimated time to completion " << elapsed_time / (i + 1) * points.size() - elapsed_time << "s");
      last_ik_bad = 0;
      last_ik_good = 0;
    }
  }
  return metrics;
}

WorkspaceMetrics WorkspaceAnalysis::computeMetricsFK(robot_state::RobotState *joint_state,
                                                     const robot_model::JointModelGroup *joint_model_group,
                                                     unsigned int max_attempts,
                                                     const ros::WallDuration &max_duration,
                                                     const std::map<std::string, std::vector<double>> &fixed_joint_values) const
{
  ros::WallTime start_time = ros::WallTime::now();
  WorkspaceMetrics metrics;
  if (!joint_model_group || !planning_scene_)
  {
    ROS_ERROR("Joint state group and planning scene should not be null");
    return metrics;
  }
  if (!fixed_joint_values.empty())
  {
    for (std::map<std::string, std::vector<double>>::const_iterator iter = fixed_joint_values.begin(); iter != fixed_joint_values.end(); ++iter)
    {
      if (!joint_model_group->hasJointModel((*iter).first))
      {
        ROS_ERROR("Could not find joint: %s in joint group: %s", (*iter).first.c_str(), joint_model_group->getName().c_str());
        return metrics;
      }
    }
  }
  metrics.group_name_ = joint_model_group->getName();
  metrics.robot_name_ = joint_state->getRobotModel()->getName();
  metrics.frame_id_ = joint_state->getRobotModel()->getModelFrame();

  //Find end-effector link
  std::string link_name = joint_model_group->getLinkModelNames().back();
  const robot_state::LinkModel *link_model = joint_state->getLinkModel(link_name);

  for (std::size_t i = 0; i < max_attempts; ++i)
  {
    if (!ros::ok() || canceled_ || (ros::WallTime::now() - start_time) >= max_duration)
      return metrics;
    joint_state->setToRandomPositions(joint_model_group);
    if (!fixed_joint_values.empty())
    {
      for (std::map<std::string, std::vector<double>>::const_iterator iter = fixed_joint_values.begin(); iter != fixed_joint_values.end(); ++iter)
      {
        joint_state->setJointPositions((*iter).first, (*iter).second);
      }
    }
    if (planning_scene_->isStateColliding(*joint_state, joint_model_group->getName()))
      continue;
    const Eigen::Affine3d &link_pose = joint_state->getGlobalLinkTransform(link_model);
    geometry_msgs::Pose pose;
    tf::poseEigenToMsg(link_pose, pose);
    metrics.points_.push_back(pose);
    updateMetrics(joint_state, joint_model_group, metrics);
  }
  return metrics;
}

void WorkspaceAnalysis::updateMetrics(robot_state::RobotState *joint_state,
                                      const robot_model::JointModelGroup *joint_model_group,
                                      moveit_workspace_analysis::WorkspaceMetrics &metrics) const
{
  double manipulability_index;
  kinematics_metrics_->getManipulabilityIndex(*joint_state,
                                              joint_model_group,
                                              manipulability_index,
                                              position_only_ik_);
  std::pair<double, const robot_model::JointModel *> distance = joint_state->getMinDistanceToPositionBounds(joint_model_group);
  std::vector<double> joint_values;
  joint_state->copyJointGroupPositions(joint_model_group, joint_values);
  metrics.joint_values_.push_back(joint_values);
  metrics.manipulability_.push_back(manipulability_index);
  metrics.min_distance_joint_limits_.push_back(distance.first);
  metrics.min_distance_joint_limit_index_.push_back(distance.second->getJointIndex());
}

bool WorkspaceMetrics::writeToFile(const std::string &filename, const std::string &delimiter, bool exclude_strings)
{
  ROS_INFO("Writing %d total points to file: %s", (int)points_.size(), filename.c_str());
  if (points_.size() != manipulability_.size() || points_.size() != joint_values_.size() || points_.size() != min_distance_joint_limits_.size())
  {
    ROS_ERROR("Workspace metrics not fully formed");
    return false;
  }

  std::ofstream file;
  file.open(filename.c_str());
  if (!file.is_open())
  {
    ROS_DEBUG("Could not open file: %s", filename.c_str());
    return false;
  }
  if (file.good())
  {
    if (!exclude_strings)
    {
      file << robot_name_ << std::endl;
      file << group_name_ << std::endl;
      file << frame_id_ << std::endl;
    }
    for (std::size_t i = 0; i < points_.size(); ++i)
    {
      if (joint_values_[i].empty())
        continue;
      file << points_[i].position.x << delimiter << points_[i].position.y << delimiter << points_[i].position.z << delimiter;
      file << points_[i].orientation.x << delimiter << points_[i].orientation.y << delimiter << points_[i].orientation.z << delimiter << points_[i].orientation.w << delimiter;
      for (std::size_t j = 0; j < joint_values_[i].size(); ++j)
        file << joint_values_[i][j] << delimiter;
      file << manipulability_[i] << delimiter << min_distance_joint_limits_[i] << delimiter << min_distance_joint_limit_index_[i] << std::endl;
      //file << manipulability_[i] << delimiter << min_distance_joint_limits_[i] << std::endl;
    }
  }
  file.close();
  return true;
}

bool WorkspaceMetrics::readFromFile(const std::string &filename, unsigned int num_joints)
{
  std::ifstream file;
  file.open(filename.c_str());
  if (!file.is_open())
  {
    ROS_DEBUG("Could not open file: %s", filename.c_str());
    return false;
  }

  std::vector<double> joint_values(num_joints);
  std::vector<std::string> name_strings;
  for (std::size_t i = 0; i < 3; ++i)
  {
    std::string name_string;
    std::getline(file, name_string);
    name_strings.push_back(name_string);
  }

  robot_name_ = name_strings[0];
  group_name_ = name_strings[1];
  frame_id_ = name_strings[2];

  while (!file.eof() && file.good())
  {
    std::string line;
    std::getline(file, line);

    std::stringstream line_stream(line);
    std::string field;
    std::vector<double> record;
    while (std::getline(line_stream, field, ','))
    {
      double f(0.0);
      std::stringstream field_stream(field);
      field_stream >> f;
      record.push_back(f);
    }
    if (record.empty())
      continue;
    ROS_DEBUG("Read: %d records", (int)record.size());
    geometry_msgs::Pose pose;
    pose.position.x = record[0];
    pose.position.y = record[1];
    pose.position.z = record[2];
    pose.orientation.x = record[3];
    pose.orientation.y = record[4];
    pose.orientation.z = record[5];
    pose.orientation.w = record[6];
    points_.push_back(pose);

    for (std::size_t i = 0; i < num_joints; ++i)
      joint_values[i] = record[7 + i];
    joint_values_.push_back(joint_values);

    manipulability_.push_back(record[7 + num_joints]);
    min_distance_joint_limits_.push_back(record[8 + num_joints]);
    min_distance_joint_limit_index_.push_back(record[9 + num_joints]);
  }
  ROS_DEBUG("Done reading");
  file.close();
  return true;
}

visualization_msgs::Marker WorkspaceMetrics::getMarker(double marker_scale, unsigned int id, const std::string &ns) const
{
  visualization_msgs::Marker marker;
  marker.type = marker.CUBE_LIST;
  marker.action = 0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = marker_scale;
  marker.scale.y = marker_scale;
  marker.scale.z = marker_scale;
  marker.id = id;
  marker.ns = ns;

  double max_manip(-1.0), min_manip(std::numeric_limits<double>::max());

  for (std::size_t i = 0; i < points_.size(); ++i)
  {
    if (manipulability_[i] > max_manip)
      max_manip = manipulability_[i];
    if (manipulability_[i] < min_manip)
      min_manip = manipulability_[i];
  }

  for (std::size_t i = 0; i < points_.size(); ++i)
  {
    marker.points.push_back(points_[i].position);
    std_msgs::ColorRGBA color;
    color.a = 0.2;
    color.g = 0.0;
    color.r = manipulability_[i] / max_manip;
    color.b = 1 - manipulability_[i] / max_manip;
    marker.colors.push_back(color);
  }
  return marker;
}

visualization_msgs::Marker WorkspaceMetrics::getDensityMarker(double marker_scale, unsigned int id, const std::string &ns, bool smooth_colors) const
{
  visualization_msgs::Marker marker;
  marker.type = marker.CUBE_LIST;
  marker.action = 0;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = marker_scale;
  marker.scale.y = marker_scale;
  marker.scale.z = marker_scale;
  marker.id = id;
  marker.ns = ns;

  std::map<point3D, int> density;

  for (std::size_t i = 0; i < points_.size(); ++i)
  {
    point3D p(points_.at(i).position.x, points_.at(i).position.y, points_.at(i).position.z);
    if (density.count(p) == 0)
      density[p] = 0;

    density[p]++;
  }

  for (auto pt : density)
  {
    geometry_msgs::Point p;
    p.x = pt.first.x;
    p.y = pt.first.y;
    p.z = pt.first.z;
    marker.points.push_back(p);
    std_msgs::ColorRGBA color;
    color.a = 1.0;
    double scale = (double)pt.second / NUM_QUATERNIONS;
    // color.g = scale>0.5?2*(scale-0.5):0.0;
    // color.r = scale<0.5?1.0-2*scale:0.0;
    // color.b = scale<0.5?2*scale:1.0-2*(scale-0.5);

    if (smooth_colors)
    {
      color.g = scale < 0.5 ? 2 * scale : 1.0;
      color.r = scale < 0.5 ? 1.0 : 1.0 - 2 * (scale - 0.5);
      color.b = scale < 0.5 ? 0.0 : 1.0 - color.r;
    }
    else
    {
      color.g = scale < 0.4 ? 0 : 1;
      color.b = scale < 0.2 ? 1 : scale < 0.8 ? 0 : 1;
      color.r = scale < 0.6 ? 1 : 0;
    }
    marker.colors.push_back(color);
  }
  return marker;
}

} // namespace moveit_workspace_analysis
