#!/usr/bin/env python

import sys
import rospy
import copy
from rospy_message_converter import message_converter
from geometry_msgs.msg import Transform, PoseStamped, Pose, Point, Quaternion
from trajectory_msgs.msg import JointTrajectory
from moveit_msgs.msg import RobotTrajectory, RobotState
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import os
import moveit_commander

from sensor_msgs.msg import JointState

from moveit_msgs.srv import GetPositionIK, GetPositionFK, GetPositionFKRequest, GetPositionFKResponse

from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
)

from std_msgs.msg import Header

import actionlib

from hamp_ros.srv import *

from hamp_ros.msg import *

from hamp_ros.tools.trajectory_editor import TrajectoryEditor

DEFAULT_FK_SERVICE = "/compute_fk"
DEFAULT_IK_SERVICE = "/compute_ik"
DEFAULT_SV_SERVICE = "/check_state_validity"

def collision_checker_client(traj):
    rospy.wait_for_service('collision_checker_service')
    try:
        collision_checker_service = rospy.ServiceProxy('collision_checker_service', CollisionCheck)
        resp = collision_checker_service(traj)
        return resp.invalid_segments, resp.invalid_segments_idxs, resp.interp_traj, resp.success
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

def time_parametrization_client(traj):
    rospy.wait_for_service('/move_group/add_time_parametrization')
    try:
        time_parametrization_srv = rospy.ServiceProxy('/move_group/add_time_parametrization', TimeParametrization)

        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        joint_state.name = arm_joints
        joint_state.position = traj.points[0].positions
        moveit_robot_state = RobotState()
        moveit_robot_state.joint_state = joint_state

        moveit_robot_traj = RobotTrajectory()
        moveit_robot_traj.joint_trajectory = traj

        resp = time_parametrization_srv(moveit_robot_state, moveit_robot_traj, "panda_arm")
        return resp.success, resp.parametrized_trajectory
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

class ForwardKinematics():
    """Simplified interface to ask for forward kinematics"""

    def __init__(self):
        rospy.loginfo("Loading ForwardKinematics class.")
        self.fk_srv = rospy.ServiceProxy(DEFAULT_FK_SERVICE, GetPositionFK)
        rospy.loginfo("Connecting to FK service")
        self.fk_srv.wait_for_service()
        rospy.loginfo("Ready for making FK calls")

    def closeFK(self):
        self.fk_srv.close()

    def getFK(self, fk_link_names, joint_names, positions, frame_id='base_link'):
        """Get the forward kinematics of a joint configuration
        @fk_link_names list of string or string : list of links that we want to get the forward kinematics from
        @joint_names list of string : with the joint names to set a position to ask for the FK
        @positions list of double : with the position of the joints
        @frame_id string : the reference frame to be used"""
        gpfkr = GetPositionFKRequest()
        if type(fk_link_names) == type("string"):
            gpfkr.fk_link_names = [fk_link_names]
        else:
            gpfkr.fk_link_names = fk_link_names
        gpfkr.robot_state.joint_state.name = joint_names
        gpfkr.robot_state.joint_state.position = positions
        gpfkr.header.frame_id = frame_id
        # fk_result = GetPositionFKResponse()
        fk_result = self.fk_srv.call(gpfkr)
        return fk_result

    def getCurrentFK(self, fk_link_names, frame_id='base_link'):
        """Get the forward kinematics of a set of links in the current configuration"""
        # Subscribe to a joint_states
        js = rospy.wait_for_message('/joint_states', JointState)
        # Call FK service
        fk_result = self.getFK(fk_link_names, js.name, js.position, frame_id)
        return fk_result

def reverse_robot_trajectory(trajectory):
    """
    Reverse the JointTrajectory inside a moveit_msgs/RobotTrajectory Message 
    and return a new moveit_msgs/RobotTrajectory Message. 

    :param traj: a moveit_msgs/RobotTrajectory Message
    """

    #Note: no Time Parameterization, just negate velocities

    traj = trajectory
    traj.joint_trajectory.points.reverse()
    end_t = traj.joint_trajectory.points[0].time_from_start
    for i, pt in enumerate(traj.joint_trajectory.points):
        pt.velocities = [-v for v in pt.velocities]
        pt.accelerations = []
        pt.effort = []
        pt.time_from_start = end_t - pt.time_from_start

    return traj 


if __name__ == '__main__':
    rospy.init_node("panda_traj_testing")

    arm_joints = ["panda_joint1", 
                "panda_joint2", 
                "panda_joint3", 
                "panda_joint4", 
                "panda_joint5",
                "panda_joint6",
                "panda_joint7"]

    home_position = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]

    second_pose = [0.5, 1.05, 1.05, 0.18, 0.68, -0.68, 0.18]

    ns = 'panda_arm_controller/'
    client = actionlib.SimpleActionClient(
        ns + "follow_joint_trajectory",
        FollowJointTrajectoryAction,
    )

    goal = FollowJointTrajectoryGoal()
    goal_time_tolerance = rospy.Time(0.1)
    goal.goal_time_tolerance = goal_time_tolerance
    server_up = client.wait_for_server(timeout=rospy.Duration(10.0))

    if not server_up:
        rospy.logerr("Timed out waiting for Joint Trajectory"
                    " Action Server to connect. Start the action server"
                    " before running example.")
        rospy.signal_shutdown("Timed out waiting for Action Server")
        sys.exit(1)


    panda_arm = moveit_commander.MoveGroupCommander("panda_arm")
    hand = moveit_commander.MoveGroupCommander("hand")
    te = TrajectoryEditor()

    panda_arm.set_joint_value_target(home_position)
    plan = panda_arm.plan()
    panda_arm.execute(plan, wait=True)
    panda_arm.stop()
    panda_arm.clear_pose_targets()


    rospy.loginfo("Initializing forward kinematics test.")
    fk = ForwardKinematics()
    # current_fk = fk.getCurrentFK('panda_gripper_center','panda_link0')
    # rospy.loginfo("Current FK:")
    # rospy.loginfo(str(current_fk.pose_stamped[0]))
    # ee_pose_pub = rospy.Publisher('pose', PoseStamped, queue_size=10)
    # rospy.sleep(2.0)
    # ee_pose_pub.publish(current_fk.pose_stamped[0])


    time_begin = rospy.Time.now()
    file_path_read = os.path.join(os.path.expanduser('~/reachability_ws/src/moveit_advanced/moveit_workspace_analysis'), 'trajectories', 'home_to_0.500000_1.050000_1.050000_0.183013_0.683013_-0.683013_0.183013plan_22.yaml') #plan_42, 22
    
    with open(file_path_read, 'r') as file_open:
        loaded_plan = yaml.load(file_open, Loader=Loader)
    #print("============ Loaded plan")

    traj_msg_converted = message_converter.convert_dictionary_to_ros_message('moveit_msgs/RobotTrajectory', loaded_plan)
    #print("============ Converted plan")
    #print(traj_msg_converted)

    orig_traj = copy.deepcopy(traj_msg_converted)

    invalid_segments, invalid_segments_idxs, interp_traj, success = collision_checker_client(orig_traj)
    
    segs_repaired = []

    for i in range(len(invalid_segments_idxs)):
        print("here" + str(i))
        idxs = invalid_segments_idxs[i]
        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        joint_state.name = arm_joints
        joint_state.position = interp_traj.joint_trajectory.points[idxs.values[0]].positions
        moveit_robot_state = RobotState()
        moveit_robot_state.joint_state = joint_state
        panda_arm.set_start_state(moveit_robot_state)

        panda_arm.set_joint_value_target(interp_traj.joint_trajectory.points[idxs.values[1]].positions)

        plan_seg = panda_arm.plan()

        segs_repaired.append(plan_seg)

    repaired_traj = te.replace_traj_segs(invalid_segments_idxs, segs_repaired, interp_traj.joint_trajectory)

    parametrization_success, repaired_parametrized_traj = time_parametrization_client(copy.deepcopy(repaired_traj))

    time_end = rospy.Time.now()

    duration = time_end - time_begin
    rospy.loginfo("HAMP: To retrieve plan, collision check and execute it took " + str(duration.to_sec()) + " secs")

    print(invalid_segments_idxs)
    print(len(invalid_segments))
    print(len(repaired_traj.points))
    print(repaired_parametrized_traj.joint_trajectory.points)
    

    """
    for i, pt in enumerate(repaired_parametrized_traj.joint_trajectory.points): #traj_msg_converted.joint_trajectory.points # #repaired_traj.points
        fk_pose = fk.getFK('panda_link6', arm_joints, pt.positions, 'panda_link0')
        ee_pose_pub.publish(fk_pose.pose_stamped[0])
        rospy.sleep(0.15)
    """

    goal.trajectory = repaired_parametrized_traj.joint_trajectory
    client.send_goal(goal)
    client.wait_for_result(timeout=rospy.Duration(15.0))

    ###############################

    panda_arm.set_start_state_to_current_state()
    panda_arm.set_joint_value_target(home_position)
    plan = panda_arm.plan()
    panda_arm.execute(plan, wait=True)
    panda_arm.stop()
    panda_arm.clear_pose_targets()
    

    #traj_reversed = reverse_robot_trajectory(traj_msg_converted)
