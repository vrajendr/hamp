#!/usr/bin/env python

import sys
import rospy
import copy
from rospy_message_converter import message_converter
from geometry_msgs.msg import Transform, PoseStamped, Pose, Point, Quaternion
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from moveit_msgs.msg import RobotTrajectory, RobotState
import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import os
import moveit_commander

from sensor_msgs.msg import JointState

from moveit_msgs.srv import GetPositionIK, GetPositionFK, GetPositionFKRequest, GetPositionFKResponse

from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
)

from std_msgs.msg import Header

import actionlib

from hamp_ros.srv import *

from hamp_ros.msg import *

from hamp_ros.tools.trajectory_editor import TrajectoryEditor

import matplotlib.pyplot as plt

import numpy as np

from  moveit_msgs.srv import GetMotionPlan
from moveit_msgs.msg import MotionPlanRequest
from moveit_msgs.msg import JointConstraint
from moveit_msgs.msg import Constraints
from moveit_msgs.srv import ExecuteKnownTrajectory
from moveit_msgs.msg import MotionPlanResponse

from moveit_python import *

DEFAULT_FK_SERVICE = "/compute_fk"
DEFAULT_IK_SERVICE = "/compute_ik"
DEFAULT_SV_SERVICE = "/check_state_validity"

def plot_results(dataset1, dataset2, dataset1_name, dataset2_name, ylabel, title, filename):

    mean_1 = np.mean(dataset1)
    std_1 = np.std(dataset1)

    mean_2 = np.mean(dataset2)
    std_2 = np.std(dataset2)

    # Create lists for the plot
    methods = [dataset1_name, dataset2_name]
    x_pos = np.arange(len(methods))
    CTEs = [mean_1, mean_2]
    error = [std_1, std_2]

    # Build the plot
    fig, ax = plt.subplots()
    ax.bar(x_pos, CTEs, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10)
    ax.set_ylabel(ylabel)
    ax.set_xticks(x_pos)
    ax.set_xticklabels(methods)
    ax.set_title(title)
    ax.yaxis.grid(True)

    # Save the figure and show
    #plt.tight_layout()
    plt.savefig(filename)
    plt.show()

def collision_checker_client(traj):
    rospy.wait_for_service('collision_checker_service')
    try:
        collision_checker_service = rospy.ServiceProxy('collision_checker_service', CollisionCheck)
        resp = collision_checker_service(traj)
        return resp.invalid_segments, resp.invalid_segments_idxs, resp.interp_traj, resp.success
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

def time_parametrization_client(traj):
    rospy.wait_for_service('/move_group/add_time_parametrization')
    try:
        time_parametrization_srv = rospy.ServiceProxy('/move_group/add_time_parametrization', TimeParametrization)

        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        joint_state.name = arm_joints
        joint_state.position = traj.points[0].positions
        moveit_robot_state = RobotState()
        moveit_robot_state.joint_state = joint_state

        moveit_robot_traj = RobotTrajectory()
        moveit_robot_traj.joint_trajectory = traj

        resp = time_parametrization_srv(moveit_robot_state, moveit_robot_traj, "panda_arm")
        return resp.success, resp.parametrized_trajectory
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

class ForwardKinematics():
    """Simplified interface to ask for forward kinematics"""

    def __init__(self):
        rospy.loginfo("Loading ForwardKinematics class.")
        self.fk_srv = rospy.ServiceProxy(DEFAULT_FK_SERVICE, GetPositionFK)
        rospy.loginfo("Connecting to FK service")
        self.fk_srv.wait_for_service()
        rospy.loginfo("Ready for making FK calls")

    def closeFK(self):
        self.fk_srv.close()

    def getFK(self, fk_link_names, joint_names, positions, frame_id='base_link'):
        """Get the forward kinematics of a joint configuration
        @fk_link_names list of string or string : list of links that we want to get the forward kinematics from
        @joint_names list of string : with the joint names to set a position to ask for the FK
        @positions list of double : with the position of the joints
        @frame_id string : the reference frame to be used"""
        gpfkr = GetPositionFKRequest()
        if type(fk_link_names) == type("string"):
            gpfkr.fk_link_names = [fk_link_names]
        else:
            gpfkr.fk_link_names = fk_link_names
        gpfkr.robot_state.joint_state.name = joint_names
        gpfkr.robot_state.joint_state.position = positions
        gpfkr.header.frame_id = frame_id
        # fk_result = GetPositionFKResponse()
        fk_result = self.fk_srv.call(gpfkr)
        return fk_result

    def getCurrentFK(self, fk_link_names, frame_id='base_link'):
        """Get the forward kinematics of a set of links in the current configuration"""
        # Subscribe to a joint_states
        js = rospy.wait_for_message('/joint_states', JointState)
        # Call FK service
        fk_result = self.getFK(fk_link_names, js.name, js.position, frame_id)
        return fk_result

if __name__ == '__main__':
    rospy.init_node("panda_intial_traj_tests")

    arm_joints = ["panda_joint1", 
                "panda_joint2", 
                "panda_joint3", 
                "panda_joint4", 
                "panda_joint5",
                "panda_joint6",
                "panda_joint7"]

    home_position = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]
    poses = []
    poses.append([0.550000, 1.050000, 1.050000, 0.970570, 0.127778, -0.161943, 0.124263]) #I accidently saved all the files with the positions in world frame instead of panda_link0
    poses.append([0.500000, 1.000000, 1.050000, -0.127778, 0.970570, -0.124263, -0.161943])
    poses.append([0.500000, 1.000000, 1.050000, -0.183013, 0.683013, -0.683013, -0.183013])
    poses.append([0.500000, 1.000000, 1.050000, -0.236268, 0.881766, -0.204124, -0.353553])
    #poses.append([0.500000, 1.000000, 1.050000, -0.497052, 0.647770, 0.351469, -0.458043])
    #poses.append([0, 1.57, 0.85, -0.37, -1.83, 2.5, 1.0])

    state = RobotTrajectory()
    point = JointTrajectoryPoint()
    point.positions = home_position
    state.joint_trajectory.points.append(point)

    time_begin = rospy.Time.now()  
    invalid_segments, invalid_segments_idxs, interp_traj, success = collision_checker_client(state)
    print(success)
    time_end = rospy.Time.now()

    duration = time_end - time_begin

    rospy.loginfo("To check one state through collision checking service it took " + str(duration.to_sec()) + " secs")

    ns = 'panda_arm_controller/'
    client = actionlib.SimpleActionClient(
        ns + "follow_joint_trajectory",
        FollowJointTrajectoryAction,
    )

    goal = FollowJointTrajectoryGoal()
    goal_time_tolerance = rospy.Time(0.1)
    goal.goal_time_tolerance = goal_time_tolerance
    server_up = client.wait_for_server(timeout=rospy.Duration(10.0))

    if not server_up:
        rospy.logerr("Timed out waiting for Joint Trajectory"
                    " Action Server to connect. Start the action server"
                    " before running example.")
        rospy.signal_shutdown("Timed out waiting for Action Server")
        sys.exit(1)

    g = MoveGroupInterface("panda_arm", "panda_link0", plan_only = True)

    """
    panda_arm = moveit_commander.MoveGroupCommander("panda_arm")
    hand = moveit_commander.MoveGroupCommander("hand")
    """
    te = TrajectoryEditor()

    g.moveToJointPosition(arm_joints, home_position, plan_only = False, planner_id = 'RRTstarkConfigDefault')

    """
    panda_arm.set_joint_value_target(home_position)
    plan = panda_arm.plan()
    panda_arm.execute(plan, wait=True)
    panda_arm.stop()
    panda_arm.clear_pose_targets()
    """

    rospy.loginfo("Initializing forward kinematics test.")
    fk = ForwardKinematics()
    current_fk = fk.getCurrentFK('panda_gripper_center','panda_link0')
    #rospy.loginfo("Current FK:")
    #rospy.loginfo(str(current_fk.pose_stamped[0]))
    ee_pose_pub = rospy.Publisher('pose', PoseStamped, queue_size=10)
    rospy.sleep(2.0)
    ee_pose_pub.publish(current_fk.pose_stamped[0])

    plans = []
    plans_idx = 0

    hamp_traj_lengths = []
    rrt_traj_lengths = []
    hamp_traj_vels = []
    rrt_traj_vels = []
    hamp_planning_times = []
    rrt_planning_times = []

    for config in poses:
        for plan_idx in range(5):
            file_path_read = os.path.join(os.path.expanduser('~/reachability_ws/src/moveit_advanced/moveit_workspace_analysis'), 
                'trajectories', 'home_to_' + str(config[0]).ljust(8, '0') + '_' + str(config[1]).ljust(8, '0') + '_' + str(config[2]).ljust(8, '0') + '_'+ 
                str(config[3]).ljust(8, '0') + '_' + str(config[4]).ljust(8, '0') + '_' + str(config[5]).ljust(8, '0') + '_' + str(config[6]).ljust(8, '0') + 'plan_' + str(plan_idx) + '.yaml')
    
            with open(file_path_read, 'r') as file_open:
                plans.append(yaml.load(file_open, Loader=Loader))
    
        time_begin = rospy.Time.now()

        shortest_length_invalid = 1000000 #JointTrajectorPoint
        shortest_traj_length = 1000000 #rad
        length_invalid = 0
        final_idxs = []
        final_plan = RobotTrajectory()

        for plan in plans:

            traj = message_converter.convert_dictionary_to_ros_message('moveit_msgs/RobotTrajectory', plan)

            invalid_segments, invalid_segments_idxs, interp_traj, success = collision_checker_client(traj)

            
            if len(invalid_segments_idxs) == 0:
                length_invalid = 0
                final_idxs = invalid_segments_idxs
                final_plan = traj
                longest_length_invalid = length_invalid
                break
            else:
            
                for idxs in invalid_segments_idxs:
                    length_invalid = length_invalid + (idxs.values[1] - idxs.values[0])

            traj_length = te.get_trajectory_avg_length(traj.joint_trajectory)
            #print(traj_length)

            if length_invalid < shortest_length_invalid and traj_length < shortest_traj_length:
                final_idxs = invalid_segments_idxs
                final_plan = interp_traj
                shortest_length_invalid = length_invalid
        
        segs_repaired = []
        #print (len(final_idxs))

        for i in range(len(final_idxs)):
            #print("here" + str(i))
            if len(final_idxs) == 0:
                break

            idxs = final_idxs[i]
            joint_state = JointState()
            joint_state.header = Header()
            joint_state.header.stamp = rospy.Time.now()
            joint_state.name = arm_joints
            joint_state.position = final_plan.joint_trajectory.points[idxs.values[0]].positions
            moveit_robot_state = RobotState()
            moveit_robot_state.joint_state = joint_state
            #panda_arm.set_start_state(moveit_robot_state)
            result = g.moveToJointPosition(arm_joints, final_plan.joint_trajectory.points[idxs.values[1]].positions, plan_only = False, start_state = moveit_robot_state)

            #panda_arm.set_joint_value_target(final_plan.joint_trajectory.points[idxs.values[1]].positions)

            #plan_seg = panda_arm.plan()

            plan_seg = result.planned_trajectory

            segs_repaired.append(plan_seg)

        repaired_traj = te.replace_traj_segs(invalid_segments_idxs, segs_repaired, final_plan.joint_trajectory)

        parametrization_success, repaired_parametrized_traj = time_parametrization_client(copy.deepcopy(repaired_traj))

        time_end = rospy.Time.now()

        duration = time_end - time_begin

        rospy.loginfo("HAMP: To retrieve plan, collision check and parametrize it took " + str(duration.to_sec()) + " secs")

        #print(repaired_parametrized_traj.joint_trajectory.points)
        
        
        for i, pt in enumerate(repaired_parametrized_traj.joint_trajectory.points): #traj_msg_converted.joint_trajectory.points # #repaired_traj.points
            fk_pose = fk.getFK('panda_link6', arm_joints, pt.positions, 'panda_link0')
            ee_pose_pub.publish(fk_pose.pose_stamped[0])
            rospy.sleep(0.15)
        

        hamp_traj_lengths.append(te.get_trajectory_avg_length(traj.joint_trajectory))
        hamp_traj_vels.append(te.get_trajectory_avg_speed(traj.joint_trajectory))
        hamp_planning_times.append(duration.to_sec())

        goal.trajectory = repaired_parametrized_traj.joint_trajectory
        #client.send_goal(goal)
        #client.wait_for_result(timeout=rospy.Duration(15.0))
        print("Executed HAMP")
        ###############################

        #panda_arm.set_start_state_to_current_state()
        #panda_arm.set_joint_value_target(home_position)
        #plan = panda_arm.plan()
        #panda_arm.execute(plan, wait=True)
        #panda_arm.stop()
        #panda_arm.clear_pose_targets()
        print("Executed go to Home")

        ################################################################################################################
        rrt_time_begin = rospy.Time.now()

        #panda_arm.set_start_state_to_current_state()
        #panda_arm.set_joint_value_target(repaired_parametrized_traj.joint_trajectory.points[-1].positions)
        #plan = panda_arm.plan()

        result = g.moveToJointPosition(arm_joints, repaired_parametrized_traj.joint_trajectory.points[-1].positions, plan_only = True, planner_id = 'RRTstarkConfigDefault')

        #rrt_traj_lengths.append(te.get_trajectory_avg_length(plan.joint_trajectory))
        #rrt_traj_vels.append(te.get_trajectory_avg_speed(plan.joint_trajectory))

        rrt_time_end = rospy.Time.now()
        rrt_duration = rrt_time_end - rrt_time_begin
        #rrt_planning_times.append(rrt_duration.to_sec())
        rospy.loginfo("RRTConnect: To retrieve plan, collision check and parametrize it took " + str(result.planning_time) + " secs")
        print(rrt_duration.to_sec())
        
        rrt_traj_lengths.append(te.get_trajectory_avg_length(result.planned_trajectory.joint_trajectory))
        rrt_traj_vels.append(te.get_trajectory_avg_speed(result.planned_trajectory.joint_trajectory))
        rrt_planning_times.append(result.planning_time) #answer.motion_plan_response.planning_time

        #panda_arm.execute(plan, wait=True)
        #panda_arm.stop()
        #panda_arm.clear_pose_targets()
        print("Executed RRT")
        ################################################################################################################
        #panda_arm.set_start_state_to_current_state()
        #panda_arm.set_joint_value_target(home_position)
        #plan = panda_arm.plan()
        #panda_arm.execute(plan, wait=True)
        #panda_arm.stop()
        #panda_arm.clear_pose_targets()
        print("Executed go to Home")

        rospy.sleep(1.0)

    plot_results(hamp_planning_times, rrt_planning_times, 'Traj-Library Lookup', 
        'RRTConnect', 'Time(s)', 'Planning Time Comparison', os.path.dirname(os.path.abspath(__file__)) + '/plots/planning_time_comparison.png')

    plot_results(hamp_traj_lengths, rrt_traj_lengths, 'Traj-Library Lookup', 
        'RRTConnect', 'Length (Rad)', 'Trajectory Length Comparison', os.path.dirname(os.path.abspath(__file__)) + '/plots/trajectory_length_comparison.png')

    plot_results(hamp_traj_vels, rrt_traj_vels, 'Traj-Library Lookup', 
        'RRTConnect', 'Length (Rad/s)', 'Trajectory Speed Comparison', os.path.dirname(os.path.abspath(__file__)) + '/plots/trajectory_speed_comparison.png')

    print(os.path.dirname(os.path.abspath(__file__)))
    print(hamp_traj_lengths)
    print(rrt_traj_lengths)

    print(hamp_traj_vels)
    print(rrt_traj_vels)

    #Path length - should be easy -> use ee_pose
    #Average trajectory speed - should be easy
    #Distance to collision
    #Planning time - should be easy
    #

    #what does state validity check
    #pics of repaired plans vs RRT