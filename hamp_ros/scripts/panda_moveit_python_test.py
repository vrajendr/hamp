#!/usr/bin/env python
import rospy
from moveit_python import *

arm_joints = ["panda_joint1", 
            "panda_joint2", 
            "panda_joint3", 
            "panda_joint4", 
            "panda_joint5",
            "panda_joint6",
            "panda_joint7"]

home_position = [0, -0.78, 0.0, -2.36, 0, 1.57, 0.78]



if __name__ == '__main__':
    rospy.init_node("panda_moveit_python_test")
    g = MoveGroupInterface("panda_arm", "panda_link0", plan_only=False)

    result = g.moveToJointPosition(arm_joints, home_position, planner_id = 'RRTkConfigDefault')

    print(result.planning_time)

    # create a planning scene interface, provide name of root link
    p = PlanningSceneInterface("panda_link0")

    p.removeCollisionObject("my_cube")

    rospy.sleep(1.0)

    # add a cube of 0.1m size, at [1, 0, 0.5] in the base_link frame
    p.addCube("my_cube", 0.05, 0.4, 0, 0.5)

    # do something

    # remove the cube
    