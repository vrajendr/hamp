#!/usr/bin/env python

import rospy
import sys
import moveit_commander
from geometry_msgs.msg import PoseStamped, Pose
from moveit_commander import MoveGroupCommander, PlanningSceneInterface
from moveit_msgs.msg import PlanningScene, ObjectColor

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from tf.transformations import quaternion_from_euler, quaternion_multiply 
from copy import deepcopy

REFERENCE_FRAME = 'panda_link0'

class MoveItCollisionObjects:
    def __init__(self):
        rospy.init_node("moveit_collision_objects")
        # Use the planning scene object to add or remove objects
        scene = PlanningSceneInterface()

        # Create a scene publisher to push changes to the scene
        #self.scene_pub = rospy.Publisher('planning_scene', PlanningScene)

        # Give each of the scene objects a unique name
        arm_1_id = 'arm_1'        
        arm_2_id = 'arm_2'
        box = 'box'
        torso = 'torso'

        # Remove leftover objects from a previous run
        scene.remove_world_object(arm_1_id)
        scene.remove_world_object(arm_2_id)
        scene.remove_world_object(box)
        scene.remove_world_object(torso)

        rospy.sleep(1)

        #arm_radius = 0.03305
        #arm_height = 0.12310

        # target_pose = PoseStamped()
        # target_pose.header.frame_id = REFERENCE_FRAME
        # target_pose.pose.position.x = 0.5
        # target_pose.pose.position.y = 0.325
        # target_pose.pose.position.z = 0.5
        # target_pose.pose.orientation.w = 1.0

        # scene.add_box(arm_1_id,target_pose,(0.05,0.7,0.05))

        # rospy.sleep(1)

        # target_pose = PoseStamped()
        # target_pose.header.frame_id = REFERENCE_FRAME
        # target_pose.pose.position.x = 0.8
        # target_pose.pose.position.y = 0.275
        # target_pose.pose.position.z = 0.5
        # target_pose.pose.orientation.w = 1.0

        # scene.add_box(arm_2_id,target_pose,(0.05,0.7,0.05))

        # rospy.sleep(1)

        """
        target_pose = PoseStamped()
        target_pose.header.frame_id = REFERENCE_FRAME
        target_pose.pose.position.x = 0.8
        target_pose.pose.position.y = 0.275
        target_pose.pose.position.z = 0.5
        target_pose.pose.orientation.w = 1.0

        scene.add_box(torso,target_pose,(0.05,0.5,0.05))

        rospy.sleep(1)
        """

        
        target_pose = PoseStamped()
        target_pose.header.frame_id = REFERENCE_FRAME
        target_pose.pose.position.x = 0.3
        target_pose.pose.position.y = 0.2
        target_pose.pose.position.z = 0.525
        target_pose.pose.orientation.w = 1.0

        scene.add_box(box,target_pose,(0.05,0.05,0.05))
        
        

if __name__ == "__main__":
    moveitCollisionObjects = MoveItCollisionObjects()