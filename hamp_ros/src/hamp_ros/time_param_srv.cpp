/**
 * Copyright (c) CTU in Prague  - All Rights Reserved
 * Created on: 10/20/14
 *     Author: Vladimir Petrik <petrivl3@fel.cvut.cz>
 *  Institute: Czech Technical University in Prague
 *    Details: Time parametrization service.;
 */

//Adapted from https://github.com/gerac83/ug_bluerobot/blob/e1c9b4083cc259642c6b38ab1e2dc8f7d3effabb/clopema_testbed/clopema_robot/src_cpp/clopema_robot/time_param_srv.cpp

#include <ros/ros.h>
#include <hamp_ros/TimeParametrization.h>
#include <moveit/robot_state/conversions.h>
#include <moveit/robot_state/robot_state.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_trajectory/robot_trajectory.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>

robot_model::RobotModelPtr kinematic_model;
float speed = 0.2;

/** \brief Check whether all names \e inames are presented in the vector \e names */
bool are_all_inputs_presented(const std::vector<std::string>& inames, const std::vector<std::string>& names) {
    for(unsigned int i = 0; i < inames.size(); ++i) {
        std::string jn = inames[i];
        bool found = std::find(names.begin(), names.end(), jn) != names.end();
        if(!found)
            return false;
    }
    return true;
}

void retimeTrajectory(trajectory_msgs::JointTrajectory& t, double factor) {
    double inv_factor = (1.0 / factor);    
    for(unsigned int i  = 0; i < t.points.size(); ++i) {
        t.points[i].time_from_start *= inv_factor;
        
        for(unsigned int j = 0; j < t.joint_names.size(); ++j) {
            t.points[i].velocities[j] *= factor;
        }
    }
}

/** \brief In case trajectory points have the same timestamp offset rest of trajectory */
void offset_timestamps(trajectory_msgs::JointTrajectory& t) {
    for(unsigned int i  = 1; i < t.points.size(); ++i) {
        double t_start = t.points[i-1].time_from_start.toSec();
        double t_end = t.points[i].time_from_start.toSec();
        if (fabs(t_end - t_start) > 0.001) {
            continue;
        }
        
        ROS_WARN_STREAM("Offseting the trajectory from position: " << i);
        //have the same time, offset rest of trajectory
        for(unsigned int j = i; j < t.points.size(); ++j) {
            t.points[j].time_from_start += ros::Duration(0.01);
        }
        i--; //check that point again it should be corrected
    }
}

/** \brief Add time parametrization to trajectory service callback method */
bool cb_parametrise(hamp_ros::TimeParametrizationRequest& req, hamp_ros::TimeParametrizationResponse& res) {

    robot_state::RobotState rs(kinematic_model);
    robot_state::robotStateMsgToRobotState(req.start_state, rs);

    robot_trajectory::RobotTrajectory rt(kinematic_model, "panda_arm");
    rt.setRobotTrajectoryMsg(rs, req.trajectory);

    trajectory_processing::IterativeParabolicTimeParameterization iptp;
    bool success = iptp.computeTimeStamps(rt);
    if(!success) {
        res.success = false;
        return true;
    }

    moveit_msgs::RobotTrajectory tmsg;
    rt.getRobotTrajectoryMsg(tmsg);
    if(!are_all_inputs_presented(req.trajectory.joint_trajectory.joint_names, tmsg.joint_trajectory.joint_names)) {
        ROS_WARN_STREAM("Some joints are missing after parametrization, are you executing trajectory on the correct groups? (more info in debug stream)");
        ROS_DEBUG_STREAM(req.trajectory.joint_trajectory);
        ROS_DEBUG_STREAM(tmsg.joint_trajectory);
        res.success = false;
        return true;
    }
    offset_timestamps(tmsg.joint_trajectory);
    //if(speed > 0 && speed < 0.21) {
    //    retimeTrajectory(tmsg.joint_trajectory, speed / 0.2);
    //}
    //offset_timestamps(tmsg.joint_trajectory);
    res.parametrized_trajectory = tmsg;
    res.success = true;
    return true;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "time_parametrization_service");
    ros::NodeHandle node("~");

    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    kinematic_model = robot_model_loader.getModel();

    ros::ServiceServer srv_parametrise = node.advertiseService("/move_group/add_time_parametrization", &cb_parametrise);

    ros::spin();
    return 0;
}