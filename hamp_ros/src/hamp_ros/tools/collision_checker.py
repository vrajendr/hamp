#!/usr/bin/env python

import copy

import rospy
from hamp_ros.msg import JointTrajectoryPointArray, IntArray, IntArray2D
from hamp_ros.srv import CollisionCheck, CollisionCheckResponse

from moveit_msgs.srv import GetStateValidityRequest, GetStateValidity
from moveit_msgs.msg import RobotState, RobotTrajectory

from sensor_msgs.msg import JointState

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from numpy import zeros, array, linspace

from math import ceil

#Adapted from https://answers.ros.org/question/203633/collision-detection-in-python/

JOINT_NAMES = ["panda_joint1", 
                "panda_joint2", 
                "panda_joint3", 
                "panda_joint4", 
                "panda_joint5",
                "panda_joint6",
                "panda_joint7"]

class CollisionChecker():
    def __init__(self):
        # prepare service for collision check
        self.sv_srv = rospy.ServiceProxy('/check_state_validity', GetStateValidity)
        # wait for service to become available
        self.sv_srv.wait_for_service()
        rospy.loginfo('service is avaiable')

    def interpolate(self, p1, p2, step_size):
        """
        @param p1 first JointTrajectoryPoint to interpolate from
        @param p2 second JointTrajectoryPoint to interpolate to
        @param step_size step size in rad for interpolation 

        @return success, JointTrajectory

        http://gazebosim.org/tutorials?tut=drcsim_ros_python&cat=drcsim
        https://github.com/gerac83/ug_bluerobot/blob/b384ec12af9793cd3b34b2d9e0346d6ebe481753/clopema_testbed/clopema_moveit/src/interpolation.cpp
        """

        position1 = array(p1.positions)
        position2 = array(p2.positions)

        if len(position1) != len(position2):
            rospy.logerror('p1 and p2 are not the same size')

        max_angle_diff = 0
        for i in range(0,len(position1)):
            angle_diff = abs(position1[i] - position2[i])
            if angle_diff > max_angle_diff:
                max_angle_diff = angle_diff

        if max_angle_diff == 0:
            return True

        t_step = ceil(max_angle_diff / step_size)

        jt = JointTrajectory()
        jt.joint_names = JOINT_NAMES

        #Debugging
        #print(t_step)
        #print(linspace(0, 1, t_step))

        for ratio in linspace(0, 1, t_step):
            interpolated_position = (1-ratio)*position1 + (ratio)*position2
            point = JointTrajectoryPoint()
            point.positions = copy.deepcopy(interpolated_position)

            jt.points.append(point)

        #print ("Interpolated Trajectory now has %d waypoints" % len(jt.points))
        return True, jt

    #https://github.com/WPI-ARC/lightning_ros/blob/master/src/tools/collision_check_service.cpp
    #Used similar logic to the i_s_thread_func
    def handle_collision_check(self, req):
        time_begin = rospy.Time.now()   
        print ("Requested trajectory has %d waypoints" % len(req.robot_traj.joint_trajectory.points))

        traj = req.robot_traj
        interp_traj = RobotTrajectory()
        interp_traj.joint_trajectory.joint_names = traj.joint_trajectory.joint_names
        
        if(len(req.robot_traj.joint_trajectory.points) == 1):
            interp_traj.joint_trajectory = req.robot_traj.joint_trajectory
        else:
            for i, point in enumerate(traj.joint_trajectory.points):

                if i == ceil((len(traj.joint_trajectory.points)-1)/2.0):
                    #interp_traj.joint_trajectory.points.append(traj.joint_trajectory.points[-1])
                    break

                p1, p2 = traj.joint_trajectory.points[i*2], traj.joint_trajectory.points[(i*2) + 1]
                interpolate_success, interpolated_traj = self.interpolate(p1, p2, 0.1)

                if interpolate_success:
                    for pt in interpolated_traj.points: 
                        interp_traj.joint_trajectory.points.append(pt)

        invalid_segments = []
        invalid_segments_idxs = []
        start_idx = 0
        end_idx = 0
        curr_invalid_segment = JointTrajectoryPointArray()
        start_invalid = True
        num_states = len(interp_traj.joint_trajectory.points)
        success = False

        for j, point in enumerate(interp_traj.joint_trajectory.points):
            
            """
            if j == ceil((num_states-1)/2.0):
                if len(curr_invalid_segment.points) != 0:
                    end_idx = j
                    idxs = IntArray()
                    idxs.values = [start_idx, end_idx]
                    invalid_segments_idxs.append(idxs)
                    invalid_segments.append(curr_invalid_segment)
                    #curr_invalid_segment = JointTrajectoryPointArray()
                break
            """

            prev_invalid_index = -1

            #Debugging
            #print (self.getStateValidity(JOINT_NAMES, pt.positions).valid)
            #print(len(curr_invalid_segment.points))

            if num_states == 1:
                success = self.getStateValidity(JOINT_NAMES, point.positions).valid
                break
            
            if self.getStateValidity(JOINT_NAMES, point.positions).valid:
                if len(curr_invalid_segment.points) != 0:
                    end_idx = j
                    idxs = IntArray()
                    idxs.values = [start_idx, end_idx]
                    invalid_segments_idxs.append(idxs)
                    invalid_segments.append(curr_invalid_segment)
                    curr_invalid_segment = JointTrajectoryPointArray()
                    start_invalid = True
                else:
                    pass
            else:
                if prev_invalid_index == (j-1) or start_invalid:
                    curr_invalid_segment.points.append(point)

                    if start_invalid == True:
                        if j == 0:
                            start_idx = 0
                        else:
                           start_idx = j-1 
                           
                        start_invalid = False

                prev_invalid_index = j
                        
        time_end = rospy.Time.now()
        duration = time_end - time_begin
        rospy.loginfo("The collision checking service took " + str(duration.to_sec()) + " secs" + " and there were " 
        + str(num_states) + " states checked and " + str(len(invalid_segments_idxs)) + " invalid segments.")

        return CollisionCheckResponse(invalid_segments, invalid_segments_idxs, interp_traj, success)

    def getStateValidity(self, joint_names, joint_positions, group_name='panda_arm', constraints=None):
        '''
        Given a RobotState and a group name and optional constraints
        return the validity of the State

        Confirmed from https://github.com/ros-visualization/puppet/blob/master/puppet_gui/nodes/test_joint_state_check.py
        that you can set a different state than current /joint_states and it should collision check properly
        '''
        gsvr = GetStateValidityRequest()

        rs = RobotState()
        rs.joint_state.name = joint_names
        rs.joint_state.position = joint_positions
        #rs.joint_state.header.stamp = rospy.Time.now()

        gsvr.robot_state = rs
        gsvr.group_name = group_name

        if constraints != None:
            gsvr.constraints = constraints

        result = self.sv_srv.call(gsvr)
        return result

    def start_server(self):
        rospy.init_node('collision_checker_server')
        rospy.Service('collision_checker_service', CollisionCheck, self.handle_collision_check)
        print("Ready to do collision checking.")
        rospy.spin()


if __name__ == '__main__':
    collision_checker_service = CollisionChecker()
    collision_checker_service.start_server()
