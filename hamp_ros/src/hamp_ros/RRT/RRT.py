#!/usr/bin/env python

import copy
import time
import random
import numpy as np
import rospy

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

from sensor_msgs.msg import JointState

from std_msgs.msg import Header

import actionlib

from hamp_ros.srv import *

from hamp_ros.msg import *

from moveit_msgs.msg import RobotTrajectory, RobotState

from control_msgs.msg import (
    FollowJointTrajectoryAction,
    FollowJointTrajectoryGoal,
)

from itertools import takewhile

from moveit_msgs.srv import GetPositionIK, GetPositionFK, GetPositionFKRequest, GetPositionFKResponse

from geometry_msgs.msg import Transform, PoseStamped, Pose, Point, Quaternion

DEFAULT_FK_SERVICE = "/compute_fk"
DEFAULT_IK_SERVICE = "/compute_ik"
DEFAULT_SV_SERVICE = "/check_state_validity"

arm_joints = ["panda_joint1", 
            "panda_joint2", 
            "panda_joint3", 
            "panda_joint4", 
            "panda_joint5",
            "panda_joint6",
            "panda_joint7"]

from moveit_python import *

def collision_checker_client(traj):
    rospy.wait_for_service('collision_checker_service')
    #print(traj)
    try:
        collision_checker_service = rospy.ServiceProxy('collision_checker_service', CollisionCheck)
        resp = collision_checker_service(traj)
        return resp.invalid_segments, resp.invalid_segments_idxs, resp.interp_traj, resp.success
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

def time_parametrization_client(traj):
    rospy.wait_for_service('/move_group/add_time_parametrization')
    try:
        time_parametrization_srv = rospy.ServiceProxy('/move_group/add_time_parametrization', TimeParametrization)

        joint_state = JointState()
        joint_state.header = Header()
        joint_state.header.stamp = rospy.Time.now()
        joint_state.name = arm_joints
        joint_state.position = traj.points[0].positions
        moveit_robot_state = RobotState()
        moveit_robot_state.joint_state = joint_state

        moveit_robot_traj = RobotTrajectory()
        moveit_robot_traj.joint_trajectory = traj

        resp = time_parametrization_srv(moveit_robot_state, moveit_robot_traj, "panda_arm")
        return resp.success, resp.parametrized_trajectory
    except rospy.ServiceException, e:
        print("Service call failed: %s"%e)

class ForwardKinematics():
    """Simplified interface to ask for forward kinematics"""

    def __init__(self):
        rospy.loginfo("Loading ForwardKinematics class.")
        self.fk_srv = rospy.ServiceProxy(DEFAULT_FK_SERVICE, GetPositionFK)
        rospy.loginfo("Connecting to FK service")
        self.fk_srv.wait_for_service()
        rospy.loginfo("Ready for making FK calls")

    def closeFK(self):
        self.fk_srv.close()

    def getFK(self, fk_link_names, joint_names, positions, frame_id='base_link'):
        """Get the forward kinematics of a joint configuration
        @fk_link_names list of string or string : list of links that we want to get the forward kinematics from
        @joint_names list of string : with the joint names to set a position to ask for the FK
        @positions list of double : with the position of the joints
        @frame_id string : the reference frame to be used"""
        gpfkr = GetPositionFKRequest()
        if type(fk_link_names) == type("string"):
            gpfkr.fk_link_names = [fk_link_names]
        else:
            gpfkr.fk_link_names = fk_link_names
        gpfkr.robot_state.joint_state.name = joint_names
        gpfkr.robot_state.joint_state.position = positions
        gpfkr.header.frame_id = frame_id
        # fk_result = GetPositionFKResponse()
        fk_result = self.fk_srv.call(gpfkr)
        return fk_result

    def getCurrentFK(self, fk_link_names, frame_id='base_link'):
        """Get the forward kinematics of a set of links in the current configuration"""
        # Subscribe to a joint_states
        js = rospy.wait_for_message('/joint_states', JointState)
        # Call FK service
        fk_result = self.getFK(fk_link_names, js.name, js.position, frame_id)
        return fk_result

class RRT():
    """
    Rapidly-Exploring Random Trees (RRT).
    """

    class Node():
        """
        A node for a doubly-linked tree structure.
        """
        def __init__(self, state, parent):
            """
            :param state: np.array of a state in the search space.
            :param parent: parent Node object.
            """
            self.state = np.asarray(state)
            self.parent = parent
            self.children = []

        def __iter__(self):
            """
            Breadth-first iterator.
            """
            nodelist = [self]
            while nodelist:
                node = nodelist.pop(0)
                nodelist.extend(node.children)
                yield node

        def __repr__(self):
            return 'Node({})'.format(', '.join(map(str, self.state)))

        def add_child(self, state):
            """
            Adds a new child at the given state.
            :param state: np.array of new child node's state
            :returns: child Node object.
            """
            child = RRT.Node(state=state, parent=self)
            self.children.append(child)
            return child

    def __init__(self,
                 start_state,
                 goal_state,
                 joint_lower_limits,
                 joint_upper_limits,
                 step_size=0.1,
                 goal_precision=0.5,
                 max_iter=10000):
        """
        :param start_state: Array representing the starting state.
        :param goal_state: Array representing the goal state.
        :param joint_lower_limits: List of lower bounds of each joint.
        :param joint_upper_limits: List of upper bounds of each joint.
        :param step_size: Distance between nodes in the RRT.
        :param goal_precision: Maximum distance between RRT and goal before
            declaring completion.
        :param sample_near_goal_prob:
        :param sample_near_goal_range:
        :param max_iter: Maximum number of iterations to run the RRT before
            failure.
        """
        self.start = RRT.Node(start_state, None)
        self.goal = RRT.Node(goal_state, None)
        self.joint_lower_limits = joint_lower_limits
        self.joint_upper_limits = joint_upper_limits
        self.step_size = step_size
        self.goal_precision = goal_precision
        self.max_iter = max_iter

        #rospy.loginfo("Initializing forward kinematics service and EE pose publisher.")
        #self.fk = ForwardKinematics()
        #self.ee_pose_pub = rospy.Publisher('pose', PoseStamped, queue_size=10)
        #rospy.sleep(2.0)

        #rospy.loginfo("Drew start_state EE pose in rviz.")
        #self._draw_ee(start_state)
        #rospy.sleep(5.0)
        #rospy.loginfo("Drew goal_state EE pose in rviz.")
        #self._draw_ee(goal_state)
        #rospy.sleep(5.0)
    def rrt_connect(self):
        q1 = self.start
        q2 = self.goal

        if self._check_for_collision(q1.state) or self._check_for_collision(q2.state):
            return None
        root1, root2 = q1 , q2
        nodes1, nodes2 = [root1], [root2]
        for _ in range(self.max_iter):
            if len(nodes1) > len(nodes2):
                nodes1, nodes2 = nodes2, nodes1
            s = self._get_random_sample()

            last1 = self.argmin(lambda n: self._distance(n.state, s), nodes1)
            for q in self._extend_sample_rrt_connect(last1.state, s):
                if self._check_for_collision(q):
                    break
                last1 = RRT.Node(q, parent=last1)
                nodes1.append(last1)

            last2 = self.argmin(lambda n: self._distance(n.state, last1.state), nodes2)
            for q in self._extend_sample_rrt_connect(last2.state, last1.state):
                if self._check_for_collision(q):
                    break
                last2 = RRT.Node(q, parent=last2)
                nodes2.append(last2)
            else:
                path1, path2 = self._trace_path_from_start(last1), self._trace_path_from_start(last2)
                if path1[0] != root1:
                    path1, path2 = path2, path1
                return configs(path1[:-1] + path2[::-1])
        return None

    """
    def rrt_connect(self):
        #TODO: check if start and goal are in collision and return right away
        tree_frequency = 1

        nodes1, nodes2 = [self.start], [self.goal]

        for k in range(self.max_iter):
            
            swap = len(nodes1) > len(nodes2)

            tree1, tree2 = nodes1, nodes2

            if swap:
                tree1, tree2 = self.goal, self.start

            last1, _ = self.extend_towards(tree1, self._get_random_sample(), self._distance, self._extend_sample_rrt_connect, self._check_for_collision,
                                    swap, tree_frequency)

            last2, success = self.extend_towards(tree2, last1.state, self._distance, self._extend_sample_rrt_connect, self._check_for_collision,
                                            not swap, tree_frequency)

            if success:
                path1, path2 = self._trace_path_from_start(last1), self._trace_path_from_start(last2)

                if swap:
                    path1, path2 = path2, path1

                return self.configs(path1[:-1] + path2[::-1])
            
        print("Failed to find path from {0} to {1} after {2} iterations!".format(
            self.start.state, self.goal.state, self.max_iter))
    """

    def argmin(self, function, sequence):
        # TODO: use min
        values = list(sequence)
        #print(sequence[0].state) 
        scores = [function(x) for x in values]
        return values[scores.index(min(scores))]

    def configs(self, nodes):
        if nodes is None:
            return None
        return list(map(lambda n: n.state, nodes))

    def negate(self, test):
        return lambda *args, **kwargs: not test(*args, **kwargs)

    def asymmetric_extend(self, q1, q2, extend_fn, backward=False):
        if backward:
            config = extend_fn(q2, q1)
            #print(config)
            return config[::-1]
        config = extend_fn(q1, q2)
        #print(config)
        return config

    def extend_towards(self, tree, target, distance_fn, extend_fn, collision_fn, swap, tree_frequency):
        last = self.argmin(lambda n: distance_fn(n.state, target), tree)
        #print(last)
        extend = list(self.asymmetric_extend(last.state, target, extend_fn, swap))
        #print(type(extend))
        safe = collision_fn(extend) #takewhile(self.negate(collision_fn), extend)
        for i, q in enumerate(safe):
            if (i % tree_frequency == 0) or (i == len(safe) - 1):
                last = Node(q, last)
                tree.add_child(last)
        success = len(extend) == len(safe)

        return last, success

    def build(self):
        """
        Build an RRT.
        In each step of the RRT:
            1. Sample a random point.
            2. Find its nearest neighbor.
            3. Attempt to create a new node in the direction of sample from its
                nearest neighbor.
            4. If we have created a new node, check for completion.
        Once the RRT is complete, add the goal node to the RRT and build a path
        from start to goal.
        :returns: A list of states that create a path from start to
            goal on success. On failure, returns None.
        """
        for k in range(self.max_iter):
            prob = random.uniform(0,1)
            if prob <= 0.8:
                sample = self._get_random_sample()
            else:
                sample = self._get_random_sample_near_goal()
            neighbor = self._get_nearest_neighbor(sample)
            new_node = self._extend_sample(sample, neighbor)
            if new_node and self._check_for_completion(new_node):
                self.goal.parent = new_node
                new_node.children.append(self.goal)
                path = self._trace_path_from_start(self.goal)
                return path

        print("Failed to find path from {0} to {1} after {2} iterations!".format(
            self.start.state, self.goal.state, self.max_iter))

    def _get_random_sample_near_goal(self):
        sample = np.asarray([random.uniform(-0.05,0.05) for i in range(7)])
        sample = np.add(sample,self.goal.state)
        return sample

    def _get_random_sample(self):
        """
        Uniformly samples the search space.
        :returns: A vector representing a randomly sampled point in the search
            space.
        """
        sample = np.asarray([random.uniform(i, j) for i,j in zip(self.joint_lower_limits,self.joint_upper_limits)])
        #print(sample)
        return sample

    def _distance(self, q1, q2):
        return np.linalg.norm(np.subtract(q1, q2))

    def _get_nearest_neighbor(self, sample): #can be improved using scipy.spatial KDTree
        """
        Finds the closest node to the given sample in the search space,
        excluding the goal node.
        :param sample: The target point to find the closest neighbor to.
        :returns: A Node object for the closest neighbor.
        """
        nnode = self.start
        ndist = np.linalg.norm(np.subtract(self.start.state, sample))
        trav = self.start.__iter__()
        for i in trav:
            dist = np.linalg.norm(np.subtract(i.state, sample))
            if ndist > dist:
                ndist = dist
                nnode = i
        return nnode

    def _extend_sample_rrt_connect(self, sample, neighbor):
        """
        Adds a new node to the RRT between neighbor and sample, at a distance
        step_size away from neighbor. The new node is only created if it will
        not collide with any of the collision objects (see
        RRT._check_for_collision)
        :param sample: target point
        :param neighbor: closest existing node to sample
        :returns: The new Node object. On failure (collision), returns None.
        """
        connect_configs = []

        while True:
            vector = np.subtract(sample, neighbor)
            mag = np.linalg.norm(vector)
            unit_v = np.divide(vector, mag)
            new_point = neighbor + np.multiply(self.step_size, unit_v)
            connect_configs.append(new_point)
            
            sample = new_point


    def _extend_sample(self, sample, neighbor):
        """
        Adds a new node to the RRT between neighbor and sample, at a distance
        step_size away from neighbor. The new node is only created if it will
        not collide with any of the collision objects (see
        RRT._check_for_collision)
        :param sample: target point
        :param neighbor: closest existing node to sample
        :returns: The new Node object. On failure (collision), returns None.
        """
        vector = np.subtract(sample, neighbor.state)
        mag = np.linalg.norm(vector)
        unit_v = np.divide(vector, mag)
        new_point = neighbor.state + np.multiply(self.step_size, unit_v)
        if not self._check_for_collision(new_point):
            #print(new_point)
            return neighbor.add_child(new_point)
        else:
            return None

    def _check_for_completion(self, node):
        """
        Check whether node is within self.step_size distance of the goal.
        :param node: The target Node
        :returns: Boolean indicating node is close enough for completion.
        """
        dist = np.linalg.norm(np.subtract(node.state, self.goal.state))
        if dist <= self.goal_precision:
            return True
        return False

    def _trace_path_from_start(self, node=None):
        """
        Traces a path from start to node, if provided, or the goal otherwise.
        :param node: The target Node at the end of the path. Defaults to
            self.goal
        :returns: A list of states (not Nodes!) beginning at the start state and
            ending at the goal state.
        """
        final_path = []
        if node == None: 
            node = self.goal
        while node:
            final_path.append(node.state)
            node = node.parent
        return final_path[::-1]

    def _check_for_collision(self, sample):
        """
        Checks if a sample point is in collision with any collision object.
        :returns: A boolean value indicating that sample is in collision.
        """
        #print(sample)
        state = RobotTrajectory()
        point = JointTrajectoryPoint()
        point.positions = sample
        state.joint_trajectory.points.append(point)
        invalid_segments, invalid_segments_idxs, interp_traj, success = collision_checker_client(state)
        return not success #service returns isStateValid? we need isStateColliding i.e. True if colliding, False if not

    #def _draw_ee(self, sample):
    #    fk_pose = self.fk.getFK('panda_link7', arm_joints, sample, 'panda_link0')
    #    #print(fk_pose)
    #    self.ee_pose_pub.publish(fk_pose.pose_stamped[0])
    #    rospy.sleep(0.1)
        
def shortcutPath(traj):
    raise NotImplementedError

def main():
    #sim = True
    rospy.init_node("panda_rrt_test")

    ns = 'panda_arm_controller/'
    client = actionlib.SimpleActionClient(
        ns + "follow_joint_trajectory",
        FollowJointTrajectoryAction,
    )

    goal = FollowJointTrajectoryGoal()
    goal_time_tolerance = rospy.Time(0.1)
    goal.goal_time_tolerance = goal_time_tolerance
    server_up = client.wait_for_server(timeout=rospy.Duration(10.0))

    if not server_up:
        rospy.logerr("Timed out waiting for Joint Trajectory"
                    " Action Server to connect. Start the action server"
                    " before running example.")
        rospy.signal_shutdown("Timed out waiting for Action Server")
        sys.exit(1)

    
    home_config = [0.0, -0.78, 0.0, -2.36, 0.0, 1.57, 0.78]

    goal_config  = [0.06, -0.34, 0.3, -2.35, 0.1, 2.0, 1.2]

    delta = 0.15
    eps = 0.1

    goal = FollowJointTrajectoryGoal()

    g = MoveGroupInterface("panda_arm", "panda_link0", plan_only=True)
    move_to_home = g.moveToJointPosition(arm_joints, home_config, planner_id = 'RRTConnectkConfigDefault')
    goal.trajectory = move_to_home.planned_trajectory.joint_trajectory
    client.send_goal(goal)
    client.wait_for_result(timeout=rospy.Duration(15.0))

    """
    move_to_goal = g.moveToJointPosition(arm_joints, goal_config, planner_id = 'RRTConnectkConfigDefault')
    print(move_to_goal.planning_time)
    goal.trajectory = move_to_goal.planned_trajectory.joint_trajectory
    client.send_goal(goal)
    client.wait_for_result(timeout=rospy.Duration(15.0))

    g = MoveGroupInterface("panda_arm", "panda_link0", plan_only=True)
    move_to_home = g.moveToJointPosition(arm_joints, home_config, planner_id = 'RRTConnectkConfigDefault')
    goal.trajectory = move_to_home.planned_trajectory.joint_trajectory
    client.send_goal(goal)
    client.wait_for_result(timeout=rospy.Duration(15.0))
    """

    Panda_RRT = RRT(
        start_state=home_config,
        goal_state=goal_config,
        joint_lower_limits = [-2.8973, -1.7628, -2.8973, -3.0718, -2.8973, -0.0175, -2.8973],
        joint_upper_limits = [2.8973, 1.7628, 2.8973, -0.0698, 2.8973, 3.7525, 2.8973], 
        step_size=delta,
        goal_precision=eps)

    rospy.sleep(1.0)

    t0 = time.clock()
    path = Panda_RRT.rrt_connect()
    #t = time.clock() - t0
    #print(str(t) + "seconds elapsed")
    if path is not None:
        #print("Path configs:")
        #print(path)
        traj = RobotTrajectory()
        traj.joint_trajectory.joint_names = arm_joints
        for config in path:
            #print(config)
            point = JointTrajectoryPoint()
            point.positions = config
            traj.joint_trajectory.points.append(point)

        #print (traj.joint_trajectory)
        #t0 = time.clock()
        parametrization_success, parametrized_traj = time_parametrization_client(traj.joint_trajectory)
        t = time.clock() - t0
        print(str(t) + "seconds elapsed")
        print(parametrized_traj.joint_trajectory)
        raw_input('Press ENTER to execute trajectory and exit')

        goal.trajectory = parametrized_traj.joint_trajectory
        client.send_goal(goal)
        client.wait_for_result(timeout=rospy.Duration(15.0))

        rospy.sleep(1.0)

if __name__ == '__main__':
    main()