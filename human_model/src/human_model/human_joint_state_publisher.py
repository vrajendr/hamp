#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header, Float64MultiArray

global joint_positions

joint_positions = [0.95, 0.82, 1.07, 0.0, 0.0, -3.14, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.35, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.52, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

def receive_joint_states(msg):
    global joint_positions
    joint_positions = msg.data

def jsp():
    rospy.init_node('human_joint_state_publisher')
    pub = rospy.Publisher('/human_1/joint_states', JointState, queue_size=10)
    
    rospy.Subscriber("/human_1/set_joint_states", Float64MultiArray, receive_joint_states, queue_size=1)

    rate = rospy.Rate(50) # 10hz
    js = JointState()
    js.header = Header()
    js.header.stamp = rospy.Time.now()
    js.name = ['PelvisTransX', 'PelvisTransY', 'PelvisTransZ', 'PelvisRotX', 'PelvisRotY', 'PelvisRotZ', 'spine_0',
  'spine_1', 'spine_2', 'neck_0', 'neck_1', 'neck_2', 'right_shoulder_0', 'right_shoulder_1', 'right_shoulder_2',
  'right_elbow_0', 'right_wrist_0', 'right_wrist_1', 'right_wrist_2', 'left_shoulder_0', 'left_shoulder_1',
  'left_shoulder_2', 'left_elbow_0', 'left_wrist_0', 'left_wrist_1', 'left_wrist_2', 'right_hip_0',
  'right_hip_1', 'right_hip_2', 'right_knee_0', 'right_ankle_0', 'right_ankle_1', 'left_hip_0',
  'left_hip_1', 'left_hip_2', 'left_knee_0', 'left_ankle_0', 'left_ankle_1']
    js.velocity = []
    js.effort = []

    while not rospy.is_shutdown():
      js.header.stamp = rospy.Time.now()
      js.position = joint_positions
      pub.publish(js)
      rate.sleep()



if __name__ == '__main__':
    jsp()