#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
import tf
import numpy as np 
from visualization_msgs.msg import Marker

def get_head_gaze_from_tf(head_tf, tf_listener):

    try:
        tf_listener.waitForTransform(head_tf,'/world',rospy.Time(),rospy.Duration(5.0))
        (head_pos, head_q) = tf_listener.lookupTransform('/world', head_tf, rospy.Time(0))
    except:
        rospy.signal_shutdown('*** Failed to get tf')
    
    return head_q, head_pos

def make_arrow(position, direction, frame):
    marker = Marker()
    marker.type = marker.ARROW
    marker.action = marker.ADD
    marker.color.r = 1
    marker.color.g = 0
    marker.color.b = 0
    marker.color.a = 1
    #settings used for thesis visibility map graphic
    # marker.scale.x = 0.5
    # marker.scale.y = 0.03
    # marker.scale.z = 0.03

    marker.scale.x = 0.2
    marker.scale.y = 0.015
    marker.scale.z = 0.015
    marker.pose.position.x = position[0]
    marker.pose.position.y = position[1]
    marker.pose.position.z = position[2]
    marker.pose.orientation.x = direction[0]
    marker.pose.orientation.y = direction[1]
    marker.pose.orientation.z = direction[2]
    marker.pose.orientation.w = direction[3]
    marker.header.frame_id = frame
    marker.header.stamp = rospy.get_rostime()
    marker.id = 0

    return marker

if __name__ == '__main__':

    rospy.init_node('human_gaze_publisher')
    markers_pub = rospy.Publisher("/gaze_vector", Marker, queue_size=1)
    tf_listener = tf.TransformListener()

    r = rospy.Rate(50) # 10hz

    while not rospy.is_shutdown():
        gazeVec, headPos = get_head_gaze_from_tf('/human_1/human/head', tf_listener)
        arrow_marker = make_arrow(headPos, gazeVec, "/world")
        markers_pub.publish(arrow_marker)
        r.sleep()

    