
#include <ros/ros.h>
#include <math.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/Octomap.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <octomap/OcTreeBase.h>
#include <octomap/octomap_types.h>
#include <dynamicEDT3D/dynamicEDTOctomap.h>
#include <chrono>

#include <human_model/HumanDistToCollision.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TransformStamped.h"
#include <tf/transform_broadcaster.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit_msgs/PlanningScene.h>

#include <sensor_msgs/JointState.h>

#include <moveit_msgs/GetPositionIK.h>


#include <eigen_conversions/eigen_msg.h>

#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>

using namespace octomap;
using namespace std;

planning_scene_monitor::PlanningSceneMonitorPtr psm;

robot_model::RobotModelPtr kinematic_model;
robot_state::RobotStatePtr kinematic_state;

ros::ServiceClient ik_client;

shared_ptr<DynamicEDTOctomap> edf_ptr = NULL;
AbstractOcTree* abs_octree = NULL;
shared_ptr<OcTree> octree = NULL;

double max_edt_dist = 2.5;
const std::string links[] = {"panda_link1", "panda_link2", "panda_link3", "panda_link4", "panda_link5", "panda_link6", "panda_link7", "panda_gripper_center"};
int num_points_interest = 8;
tf2_ros::Buffer tfBuffer;

bool init_dist_map = true;
bool resetEDT = false;

void cb_resetEDT(const std_msgs::Empty::ConstPtr& msg) {
  resetEDT = true;
}

void octomap_callback(const octomap_msgs::Octomap& msg){

    if (init_dist_map || resetEDT){
        abs_octree=octomap_msgs::binaryMsgToMap(msg);
        octree.reset(dynamic_cast<octomap::OcTree*>(abs_octree));
    }
    
    ROS_INFO_ONCE("[Objects handler] octomap received.");
    double x,y,z;
    octree.get()->getMetricMin(x,y,z);
    // cout<<"metric min : "<<x<<" , "<<y << ", " << z << endl;
    octomap::point3d boundary_min(x,y,z); 
    octree.get()->getMetricMax(x,y,z);
    // cout<<"metric max : "<<x<<" , "<<y << ", " << z << endl;
    octomap::point3d boundary_max(x,y,z); 

    boundary_min.x()=-max_edt_dist;
    boundary_min.y()=-max_edt_dist;
    boundary_min.z()=-max_edt_dist;

    boundary_max.x()=max_edt_dist;
    boundary_max.y()=max_edt_dist;
    boundary_max.z()=max_edt_dist;
    
    bool unknownAsOccupied = false;

    if (init_dist_map || resetEDT){
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        if (init_dist_map){
            std::cout<<"Initializing EDT!"<<std::endl;
        }
        else if (resetEDT){
            std::cout<<"Resetting EDT!"<<std::endl;
        }

        //Euclidean distance transform  
        edf_ptr.reset (new DynamicEDTOctomap(max_edt_dist,octree.get(),
            boundary_min,
            boundary_max,unknownAsOccupied));

        edf_ptr.get()->update();          
        // point3d eval_point(0.1775,0.9987,1.4101);        
        // cout<<"eval dist val of [0.1775,0.9987,1.4101] = "<<edf_ptr.get()->getDistance(eval_point)<<endl;

        // std::cout << "fun: edf_ptr.use_count() == " << edf_ptr.use_count() << endl; 

        // while (edf_ptr != NULL){
        //     point3d eval_point(0.1775,0.9987,1.4101);        
        //     cout<<"eval dist val of [0.1775,0.9987,1.4101] = "<<edf_ptr.get()->getDistance(eval_point)<<endl;
        // }
        // bool edf_consistent = edf_ptr.get()->checkConsistency();
        // cout<<"EDF Consistent?: "<<edf_consistent<<endl;

        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        double diff = std::chrono::duration_cast<chrono::nanoseconds>( end - begin ).count()*1e-9;
        ROS_INFO("[EDT build] dynamic EDT computed in %f [sec]",diff);
        init_dist_map = false;
        resetEDT = false;
    }


};

double humanVisibilityAngle(double x, double y, double z){

    geometry_msgs::TransformStamped world_to_head;
    tf::Transform TFWH;

    world_to_head = tfBuffer.lookupTransform("world", "human_1/human/head", ros::Time(0), ros::Duration(1.0));

    // the received data is a geometry_msgs/Transform, we need to convert it to tf::Transform.
    tf::transformMsgToTF(world_to_head.transform, TFWH); // don't care about stamp
    
    // Debugging
    //printTransform(TFWH);

    tf::Vector3 gazeVec(TFWH.getBasis()[0][0], TFWH.getBasis()[1][0], TFWH.getBasis()[2][0]); //vector along x-axis is the gaze direction
    tf::Vector3 pointVec(x-TFWH.getBasis()[3][0], y-TFWH.getBasis()[3][1], z-TFWH.getBasis()[3][2]);

    // Debugging
    //printVector3(gazeVec.normalize());

    gazeVec.normalize();
    pointVec.normalize();

    tfScalar beta = gazeVec.dot(pointVec);

    tfScalar angle = tfAcos(beta);

    return double(angle);
}

void jointStatesCallback(const sensor_msgs::JointState &joint_states_current){

    //not sure why this doesn't update in the for loop? but just hardcode now from panda_rrtconnect_test4_apper.py
    kinematic_state->setVariablePosition("PelvisTransX",0.828000000000003);
    kinematic_state->update();
    kinematic_state->setVariablePosition("PelvisTransY",0.8880000000000017);
    kinematic_state->update();
    kinematic_state->setVariablePosition("PelvisTransZ",1.0620000000000047);
    kinematic_state->update();
    kinematic_state->setVariablePosition("PelvisRotX",0.0);
    kinematic_state->update();
    kinematic_state->setVariablePosition("PelvisRotY",0.0);
    kinematic_state->update();
    kinematic_state->setVariablePosition("PelvisRotZ",-3.1405499999999997);
    kinematic_state->update();

    //update RobotStatePtr for human model from the human's joint states
    for (size_t i = 0; i < joint_states_current.position.size(); ++i){
        kinematic_state->setVariablePosition(joint_states_current.name[i], joint_states_current.position[i]);
        kinematic_state->update();
    }
}

bool in_array(const std::string &value, const std::vector<std::string> &array){
    return std::find(array.begin(), array.end(), value) != array.end();
}


bool cb_distToCollision(human_model::HumanDistToCollisionRequest& req, human_model::HumanDistToCollisionResponse& res) {

    std::vector<double> link_distances;
    std::vector<double> vis_angles;
    float dist;

    planning_scene_monitor::LockedPlanningSceneRW locked_scene(psm);
    const planning_scene::PlanningSceneConstPtr ps= static_cast<const planning_scene::PlanningSceneConstPtr>(locked_scene);

    robot_state::RobotStatePtr robot_state(new robot_state::RobotState(ps->getCurrentState()));

    std::vector<double> joint_values = req.joint_values;
    const robot_model::JointModelGroup* joint_model_group = robot_state->getJointModelGroup(req.group);
    robot_state->setJointGroupPositions(joint_model_group, joint_values);
    robot_state->update(true);

    for(int i=0;i<num_points_interest;i++){
        const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform(links[i]);
        Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
        octomap::point3d p(ee_trans[0],ee_trans[1],ee_trans[2]);

        // std::lock_guard<std::mutex> lock(dist_map_mutex);
        dist = edf_ptr.get()->getDistance(p);
        // std::cout<<"\n\ndistance at point "<<p.x()<<","<<p.y()<<","<<p.z()<<" is "<<dist<<std::endl;
        if (dist > max_edt_dist){
            dist = max_edt_dist;
        }
        else if (dist < - max_edt_dist){
            dist = -max_edt_dist;
        }
        if (dist == DynamicEDTOctomap::distanceValue_Error){
            dist = 0.01;
        }
        link_distances.push_back(dist);
    }

    //--------------------------Calculate Visibility Angle to Human of Panda End Effector-----------------------//
    // const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform("panda_gripper_center"); 
    // Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
    // double vis_angle = humanVisibilityAngle(ee_trans[0], ee_trans[1], ee_trans[2]);

    for(int i=0;i<num_points_interest;i++){
        const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform(links[i]);
        Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
        vis_angles.push_back(humanVisibilityAngle(ee_trans[0], ee_trans[1], ee_trans[2]));
    }
    //--------------------------Calculate Visibility Angle to Human of Panda End Effector-----------------------//

    //--------------------------Calculate human model IK to panda_gripper_center-----------------------//
    // robot_model_loader::RobotModelLoader robot_model_loader("human_1/robot_description");
    // const moveit::core::RobotModelPtr& kinematic_model = robot_model_loader.getModel();
    // ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());

    // moveit::core::RobotStatePtr kinematic_state(new moveit::core::RobotState(kinematic_model));
    // kinematic_state->setToDefaultValues();
    const moveit::core::JointModelGroup* human_joint_model_group = kinematic_model->getJointModelGroup("left_arm");

    const std::vector<std::string>& human_joint_names = human_joint_model_group->getVariableNames();

    // std::vector<double> human_joint_values;
    // kinematic_state->copyJointGroupPositions(human_joint_model_group, human_joint_values);
    // for (std::size_t i = 0; i < human_joint_names.size(); ++i)
    // {
    //     ROS_INFO("Joint %s: %f", human_joint_names[i].c_str(), human_joint_values[i]);
    // }

    // ROS_INFO("Joint %s: %f", "PelvisTransX", kinematic_state->getJointModel("PelvisTransX"));
    // ROS_INFO("Joint %s: %f", "PelvisTransY", kinematic_state->getJointModel("PelvisTransY"));
    // ROS_INFO("Joint %s: %f", "PelvisTransZ", kinematic_state->getJointModel("PelvisTransZ"));
    
    //panda_gripper_center location
    const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform(links[7]);
    Eigen::Affine3d end_effector_state_non_const = end_effector_state;
    
    // const auto &solver = human_joint_model_group->getSolverInstance();
    // kinematic_state->setToIKSolverFrame(end_effector_state_non_const, solver);

    // double timeout = 2.0;
    // bool found_ik = kinematic_state->setFromIK(human_joint_model_group, end_effector_state, timeout);

    double joint_angle_dist_cost = 0.0;
    double joint_limit_dist_cost = 0.0;
    double arm_potential = 0.0;

    double comfort_cost;

    double human_mass = 65.0; //this is in URDF as well
    double arm_mass_fraction = 0.04715;
    double m = human_mass*arm_mass_fraction;
    double g = 9.8; 

    // if (found_ik)
    // {
    //     kinematic_state->copyJointGroupPositions(human_joint_model_group, human_joint_values);
    //     for (std::size_t i = 0; i < human_joint_names.size(); ++i)
    //     {
    //         // ROS_INFO("Joint %s: %f", human_joint_names[i].c_str(), human_joint_values[i]);
    //         joint_angle_dist_cost += pow(human_joint_values[i],2);
    //         const moveit::core::JointModel* joint =  kinematic_state->getJointModel(human_joint_names[i]);
    //         std::vector<const moveit::core::JointModel*> jointContainer;
    //         jointContainer.push_back(joint);
    //         joint_limit_dist_cost += pow(kinematic_state->getMinDistanceToPositionBounds(jointContainer).first,2);
    //     }
    // }
    // else
    // {
    //     ROS_INFO("Did not find IK solution");
    // }
    
    moveit_msgs::GetPositionIK::Request ik_req;
    moveit_msgs::GetPositionIK::Response ik_res;

    geometry_msgs::Pose ik_pose;
    
    tf::poseEigenToMsg(end_effector_state_non_const, ik_pose);

    tf::Matrix3x3 obs_mat;
    obs_mat.setEulerYPR(3.1415,0.0,0.0);

    tf::Quaternion q_tf;
    obs_mat.getRotation(q_tf);

    //debug pose
    // ROS_INFO("ik_pose: %f, %f, %f ", ik_pose.position.x, ik_pose.position.y, ik_pose.position.z);

    ik_req.ik_request.timeout = ros::Duration(0.001);
    ik_req.ik_request.group_name = "left_arm";
    ik_req.ik_request.pose_stamped.header.frame_id = "/world";
    ik_req.ik_request.pose_stamped.header.stamp = ros::Time::now();
    ik_req.ik_request.pose_stamped.pose.position = ik_pose.position;
    ik_req.ik_request.pose_stamped.pose.orientation.x = q_tf.getX();
    ik_req.ik_request.pose_stamped.pose.orientation.y = q_tf.getY();
    ik_req.ik_request.pose_stamped.pose.orientation.z = q_tf.getZ();
    ik_req.ik_request.pose_stamped.pose.orientation.w = q_tf.getW();
    // ik_req.ik_request.robot_state.is_diff = true;

    if(ik_client.call(ik_req, ik_res) && ik_res.error_code.val == ik_res.error_code.SUCCESS){

        for (size_t i = 0; i < ik_res.solution.joint_state.name.size(); ++i){

            kinematic_state->setVariablePosition(ik_res.solution.joint_state.name[i], ik_res.solution.joint_state.position[i]);
            kinematic_state->update();

            if(in_array(ik_res.solution.joint_state.name[i], human_joint_names)){
                joint_angle_dist_cost += pow(ik_res.solution.joint_state.position[i],2);
                const moveit::core::JointModel* joint =  kinematic_state->getJointModel(ik_res.solution.joint_state.name[i]);
                std::vector<const moveit::core::JointModel*> jointContainer;
                jointContainer.push_back(joint);
                joint_limit_dist_cost += pow(kinematic_state->getMinDistanceToPositionBounds(jointContainer).first,2);
            }

        }

        double h = ik_pose.position.z - 0.8; //a very crude approximation... but serves the idea well, rest position the arm is at 0.8m in Z
        arm_potential = m*g*h;

        comfort_cost = (arm_potential/20.0 + joint_limit_dist_cost/14.0 + joint_angle_dist_cost/3.0)/3.0;
    }
    else {
        comfort_cost = 1.0;
        ROS_INFO("Ik failed with response: %d ", ik_res.error_code.val);
    }

    //--------------------------Calculate human model IK to panda_gripper_center-----------------------//

    res.collision = !ps->isStateColliding(*robot_state, req.group, false/*verbose*/);
    res.link_distances = link_distances;
    res.vis_angles = vis_angles;

    //last minute hack
    res.left_arm_torso_comfort_cost =  comfort_cost;

    //ROS_INFO("Comfort cost: %f", res.left_arm_torso_comfort_cost);
    // ros::Time end = ros::Time::now();
    // ros::Duration duration = end-start;
    // std::cout<<"\n\nelapsted time in service is "<<duration.toSec()<<" secs "<<std::endl;

    return true;
}


int main(int argc,char** argv){
    ros::init(argc,argv,"human_model_data_srv");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(0); //Choose how many threads
    spinner.start();

    psm = std::make_shared<planning_scene_monitor::PlanningSceneMonitor>("robot_description");
    psm->startStateMonitor();
    psm->startSceneMonitor("move_group/monitored_planning_scene");

    tf2_ros::TransformListener tf2_listener(tfBuffer);

    ros::Subscriber sub_octo = nh.subscribe("/octomap_full",1,octomap_callback);
    ros::ServiceServer srv_distToCollision = nh.advertiseService("/human_model/distance_to_collision", cb_distToCollision);
    ros::Subscriber resetEDTSub = nh.subscribe("human_model/reset_edt", 1000, cb_resetEDT);

    //ros::Subscriber joint_states_sub = nh.subscribe("/human_1/joint_states", 100, jointStatesCallback);

    ik_client = nh.serviceClient<moveit_msgs::GetPositionIK>("human_1/compute_ik");

    robot_model_loader::RobotModelLoader robot_model_loader("human_1/robot_description");
    kinematic_model = robot_model_loader.getModel();
    kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));

    ros::Rate spin_rate(25);

    while (ros::ok()) {
        // if (edf_ptr != NULL){
        //     point3d eval_point(0.1775,0.9987,1.4101);        
        //     cout<<"eval dist val of [0.1775,0.9987,1.4101] = "<<edf_ptr.get()->getDistance(eval_point)<<endl;
        // }

        ros::spinOnce();    // Process callbacks.
        spin_rate.sleep();  // Sleep until next cycle.
    }
    ros::waitForShutdown();
    return 0;
}