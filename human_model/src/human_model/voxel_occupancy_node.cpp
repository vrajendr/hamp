#include <human_model/scene_subscriber.hpp>

#include <vector>
#include <string>
#include <mutex>

#include <octomap_msgs/conversions.h>
#include <octomap/octomap.h>
#include <ros/ros.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit_msgs/PlanningScene.h>

using namespace human_model;

//#include <ros/node_handle.h>
#include <visualization_msgs/MarkerArray.h>

#include <dynamicEDT3D/dynamicEDT3D.h>
#include <dynamicEDT3D/dynamicEDTOctomap.h>

#include <opencv/cv.h>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <human_model/HumanDistToCollision.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>
#include "geometry_msgs/PoseStamped.h"

#include "geometry_msgs/TransformStamped.h"

#include <tf/transform_broadcaster.h>

#include "std_srvs/Empty.h"
#include <std_msgs/Empty.h>

#define _USE_MATH_DEFINES
#include <math.h>
using namespace cv;


std::mutex dist_map_mutex;

planning_scene_monitor::PlanningSceneMonitorPtr psm;

std::unique_ptr<DynamicEDTOctomap> dist_map_ptr;

bool init_dist_map = true;

double max_edt_dist = 2.5;

const std::string links[] = {"panda_link1", "panda_link2", "panda_link3", "panda_link4", "panda_link5", "panda_link6", "panda_link7", "panda_gripper_center"};
int num_points_interest = 8;

tf2_ros::Buffer tfBuffer;

bool resetEDT = false;

void cb_resetEDT(const std_msgs::Empty::ConstPtr& msg) {
  resetEDT = true;
}

template<typename to, typename from>
to lexical_cast(from const &x)
{
    std::stringstream os;
    to ret;
    os << x;
    os >> ret;
    return ret;
}

double humanVisibilityAngleOld(double x, double y, double z){

    geometry_msgs::TransformStamped world_to_head;

    world_to_head = tfBuffer.lookupTransform("world", "human_1/human/head", ros::Time(0), ros::Duration(1.0));

    double headLoc[3];

    double gazeVec[3];

    double pitch, yaw, roll;

    headLoc[0] = world_to_head.transform.translation.x;
    headLoc[1] = world_to_head.transform.translation.y;
    headLoc[2] = world_to_head.transform.translation.z;


    //calculate the pitch and yaw angles that the human head would need to move in order to view point x,y,z
    //NOTE: this currently only works for 3D position of the head and needs some further math to account for 6D positions
    gazeVec[0] = x-headLoc[0];
    gazeVec[1] = y-headLoc[1]; 
    gazeVec[2] = z-headLoc[2];

    pitch = atan2(gazeVec[2],pow(pow(gazeVec[0],2)+pow(gazeVec[1],2),0.5));
    yaw = atan2(gazeVec[1],gazeVec[0]) + M_PI;
    roll = 0;

    tf2::Quaternion q;
    q.setRPY(roll, pitch, yaw);
    q.normalize();

    double angle = q.getAngleShortestPath();
    //q.getAngleShortestPath() returns a value between [0, pi]
    // std::cout<<"\n\nVisibility angle at point "<<x<<","<<y<<","<<z<<" is "<<angle<<std::endl;
    return angle;
}

//https://github.com/mwegiere/serwo
void printTransform(tf::Transform  T)
{
    printf("[ %f %f %f %f \n %f %f %f %f \n %f %f %f %f \n %f %f %f %f]\n", T.getBasis()[0][0], T.getBasis()[0][1], T.getBasis()[0][2], T.getBasis()[0][3],
            T.getBasis()[1][0], T.getBasis()[1][1], T.getBasis()[1][2], T.getBasis()[1][3],
            T.getBasis()[2][0], T.getBasis()[2][1], T.getBasis()[2][2], T.getBasis()[2][3],
            T.getBasis()[3][0], T.getBasis()[3][1], T.getBasis()[3][2], T.getBasis()[3][3]);
}

void printVector3(tf::Vector3 V)
{
    printf("[ %f %f %f ]\n", V.getX(), V.getY(), V.getZ());
}

double humanVisibilityAngle(double x, double y, double z){

    geometry_msgs::TransformStamped world_to_head;
    tf::Transform TFWH;

    world_to_head = tfBuffer.lookupTransform("world", "human_1/human/head", ros::Time(0), ros::Duration(1.0));

    // the received data is a geometry_msgs/Transform, we need to convert it to tf::Transform.
    tf::transformMsgToTF(world_to_head.transform, TFWH); // don't care about stamp
    
    // Debugging
    //printTransform(TFWH);

    tf::Vector3 gazeVec(TFWH.getBasis()[0][0], TFWH.getBasis()[1][0], TFWH.getBasis()[2][0]); //vector along x-axis is the gaze direction
    tf::Vector3 pointVec(x-TFWH.getBasis()[3][0], y-TFWH.getBasis()[3][1], z-TFWH.getBasis()[3][2]);

    // Debugging
    //printVector3(gazeVec.normalize());

    gazeVec.normalize();
    pointVec.normalize();

    tfScalar beta = gazeVec.dot(pointVec);

    tfScalar angle = tfAcos(beta);

    return double(angle);
}

double humanVisibilityCost(double angle){
    //angle is [0, pi] so just normalize angle to get a cost [0, 1]
    double vis_cost = angle/M_PI;
    
    return vis_cost;
}

// bool cb_updateDistMap(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response) {
//     dist_map_ptr.reset(new DynamicEDTOctomap (float(max_edt_dist), const_cast<octomap::OcTree*>(&*octomap), min, max, unknownAsOccupied));
//     dist_map_ptr->update();
//     return true;
// }

bool cb_distToCollision(human_model::HumanDistToCollisionRequest& req, human_model::HumanDistToCollisionResponse& res) {
    // ros::Time start = ros::Time::now();

    std::vector<double> link_distances;
    std::vector<double> vis_angles;
    float dist;

    planning_scene_monitor::LockedPlanningSceneRW locked_scene(psm);
    const planning_scene::PlanningSceneConstPtr ps= static_cast<const planning_scene::PlanningSceneConstPtr>(locked_scene);

    robot_state::RobotStatePtr robot_state(new robot_state::RobotState(ps->getCurrentState()));

    std::vector<double> joint_values = req.joint_values;
    const robot_model::JointModelGroup* joint_model_group = robot_state->getJointModelGroup(req.group);
    robot_state->setJointGroupPositions(joint_model_group, joint_values);
    robot_state->update(true);

    for(int i=0;i<num_points_interest;i++){
        const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform(links[i]);
        Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
        octomap::point3d p(ee_trans[0],ee_trans[1],ee_trans[2]);

        // std::lock_guard<std::mutex> lock(dist_map_mutex);
        dist = dist_map_ptr.get()->getDistance(p);
        // std::cout<<"\n\ndistance at point "<<p.x()<<","<<p.y()<<","<<p.z()<<" is "<<dist<<std::endl;
        if (dist > max_edt_dist){
            dist = max_edt_dist;
        }
        else if (dist < - max_edt_dist){
            dist = -max_edt_dist;
        }
        if (dist == DynamicEDTOctomap::distanceValue_Error){
            dist = 0.01;
        }
        link_distances.push_back(dist);
    }

    //--------------------------Calculate Visibility Angle to Human of Panda End Effector-----------------------//
    // const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform("panda_gripper_center"); 
    // Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
    // double vis_angle = humanVisibilityAngle(ee_trans[0], ee_trans[1], ee_trans[2]);

    for(int i=0;i<num_points_interest;i++){
        const Eigen::Affine3d& end_effector_state = robot_state->getGlobalLinkTransform(links[i]);
        Eigen::Transform<double, 3, 2>::ConstTranslationPart ee_trans=end_effector_state.translation();
        vis_angles.push_back(humanVisibilityAngle(ee_trans[0], ee_trans[1], ee_trans[2]));
    }
    //--------------------------Calculate Visibility Angle to Human of Panda End Effector-----------------------//


    res.collision = !ps->isStateColliding(*robot_state, req.group, false/*verbose*/);
    res.link_distances = link_distances;
    res.vis_angles = vis_angles;

    // ros::Time end = ros::Time::now();
    // ros::Duration duration = end-start;
    // std::cout<<"\n\nelapsted time in service is "<<duration.toSec()<<" secs "<<std::endl;

    return true;
}

double distanceBtwnPoseStamps(const geometry_msgs::PoseStamped &pose1, const geometry_msgs::PoseStamped &pose2){
  const geometry_msgs::Point &p1 = pose1.pose.position;
  const geometry_msgs::Point &p2 = pose2.pose.position;
  const double dx = p1.x - p2.x;
  const double dy = p1.y - p2.y;
  const double dz = p1.z - p2.z;
  return sqrt(dx * dx + dy * dy + dz * dz);
}

visualization_msgs::Marker makeVisibilityMarkers(){

    // geometry_msgs::TransformStamped world_to_head;

    // world_to_head = tfBuffer.lookupTransform("world", "human_1/human/head", ros::Time(0), ros::Duration(1.0));
    //std::cout<< world_to_head <<std::endl;

    float distance;

    double x_min,y_min,z_min, x_max,y_max,z_max;

    x_min=-1;
    y_min=0.9;
    z_min=-0.2;

    x_max=3;
    y_max=2;
    z_max=3;

    // x_min= 0;
    // y_min= 0;
    // z_min= 0;

    // x_max=3;
    // y_max=3;
    // z_max=3;

    // x_min= 0;
    // y_min= 0;
    // z_min= 1;

    // x_max=1;
    // y_max=1.5;
    // z_max=3;

    //Use values below to easily debug (it produces markers along the world Z-Axis)
    // x_min= 0;
    // y_min= 0;
    // z_min= 1;
    // x_max=0.2;
    // y_max=0.2;
    // z_max=3;

    //Use values below to make a vertical line of markers directly in front of the human
    // x_min= 0;
    // y_min= 0.8;
    // z_min= 1;
    // x_max=0.2;
    // y_max=1;
    // z_max=3;

    std::vector<float> angles_vector;

    double step_size=0.1;

    for(double x=x_min;x<x_max;x=x+step_size)
    {
        for(double y=y_min;y<y_max;y=y+step_size)
        {
            for(double z=z_min;z<z_max;z=z+step_size)
            {
                double angle = humanVisibilityAngle(x, y, z);

                //std::cout<<"\n\nAngle at point "<<x<<","<<y<<","<<z<<" is "<<angle<<std::endl;
                angles_vector.push_back(M_PI - angle);
                
            }
        }
    }

    cv::Mat mat_raw_distances_vector(angles_vector);

    cv::Mat mat_normilized_distances_value(mat_raw_distances_vector.rows,mat_raw_distances_vector.cols ,CV_8UC1);
    cv::normalize( mat_raw_distances_vector, mat_normilized_distances_value, 0, 255, cv::NORM_MINMAX, CV_8UC1, cv::Mat() );

    cv::Mat cm_img0;

    cv::applyColorMap(mat_normilized_distances_value, cm_img0,cv::COLORMAP_RAINBOW);

    std::vector<cv::Mat> bgr_planes;
    cv::split( cm_img0, bgr_planes );

    cv::Mat blue, green, red;

    blue=bgr_planes.at(0);
    green=bgr_planes.at(1);
    red=bgr_planes.at(2);

    std::string frame_id="world";
    
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time();
    marker.ns = "human_model";
    marker.id = lexical_cast<int>(1);
    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.action = visualization_msgs::Marker::ADD;

    int i=0;
    for(double x=x_min;x<x_max;x=x+step_size)
    {
        for(double y=y_min;y<y_max;y=y+step_size)
        {
            for(double z=z_min;z<z_max;z=z+step_size)
            {
                geometry_msgs::Point point;
                std_msgs::ColorRGBA color;

                color.a=1.0;
                point.x=x;
                point.y=y;
                point.z=z;

                color.r=(red.at<uchar>(i,0)*1.0 )/255.0;
                color.g=(green.at<uchar>(i,0)*1.0) /255.0;
                color.b=(blue.at<uchar>(i,0)*1.0 )/255.0 ;

                marker.points.push_back(point);
                marker.colors.push_back(color);
                marker.scale.x=0.11;
                marker.scale.y=0.11;
                marker.scale.z=0.11;
                i++;
            }
        }
    }

    return marker;
}

visualization_msgs::Marker makeEDTMarkers(){
    //This is how you can query the map
    octomap::point3d p;

    float distance;

    double x_min,y_min,z_min, x_max,y_max,z_max;

    // x_min=-1;
    // y_min=0.9;
    // z_min=0;

    // x_max=2;
    // y_max=2;
    // z_max=2;
    
    x_min=-1;
    y_min=0.9;
    z_min=-0.2;

    x_max=3;
    y_max=2;
    z_max=3;

    std::vector<float> distances_vector;

    double step_size=0.1;

    for(double x=x_min;x<x_max;x=x+step_size)
    {
        for(double y=y_min;y<y_max;y=y+step_size)
        {
            for(double z=z_min;z<z_max;z=z+step_size)
            {
                octomap::point3d p(x,y,z);
                distances_vector.push_back(dist_map_ptr.get()->getDistance(p));
                //distances_vector.push_back(distmap.getDistance(p));
                //std::cout<< dist_map_ptr->getDistance(p) <<std::endl;
            }
        }
    }

    cv::Mat mat_raw_distances_vector(distances_vector);

    cv::Mat mat_normilized_distances_value(mat_raw_distances_vector.rows,mat_raw_distances_vector.cols ,CV_8UC1);
    cv::normalize( mat_raw_distances_vector, mat_normilized_distances_value, 0, 255, cv::NORM_MINMAX, CV_8UC1, cv::Mat() );

    cv::Mat cm_img0;

    cv::applyColorMap(mat_normilized_distances_value, cm_img0,cv::COLORMAP_RAINBOW);

    std::vector<cv::Mat> bgr_planes;
    cv::split( cm_img0, bgr_planes );

    cv::Mat blue, green, red;

    blue=bgr_planes.at(0);
    green=bgr_planes.at(1);
    red=bgr_planes.at(2);

    std::string frame_id="world";
    
    visualization_msgs::Marker marker;
    marker.header.frame_id = frame_id;
    marker.header.stamp = ros::Time();
    marker.ns = "human_model";
    marker.id = lexical_cast<int>(1);
    marker.type = visualization_msgs::Marker::SPHERE_LIST;
    marker.action = visualization_msgs::Marker::ADD;

    int i=0;
    for(double x=x_min;x<x_max;x=x+step_size)
    {
        for(double y=y_min;y<y_max;y=y+step_size)
        {
            for(double z=z_min;z<z_max;z=z+step_size)
            {
                geometry_msgs::Point point;
                std_msgs::ColorRGBA color;

                color.a=1.0;
                point.x=x;
                point.y=y;
                point.z=z;

                color.r=(red.at<uchar>(i,0)*1.0 )/255.0;
                color.g=(green.at<uchar>(i,0)*1.0) /255.0;
                color.b=(blue.at<uchar>(i,0)*1.0 )/255.0 ;

                marker.points.push_back(point);
                marker.colors.push_back(color);
                marker.scale.x=0.11;
                marker.scale.y=0.11;
                marker.scale.z=0.11;
                i++;
            }
        }
    }

    return marker;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "human_model_voxel_occupancy");

  //ros::AsyncSpinner spinner(0); //Choose how many threads
  //spinner.start();

//   psm = std::make_shared<planning_scene_monitor::PlanningSceneMonitor>("robot_description");
//   psm->startStateMonitor();
//   psm->startSceneMonitor("move_group/monitored_planning_scene");
  
//   tf2_ros::TransformListener tf2_listener(tfBuffer);

  // Create subscriber and start spinning
  std::shared_ptr<SceneSubscriber> subscriber = CreateFromRosParams();
  subscriber->Start();

  // Create octomap publisher
  std::shared_ptr<ros::NodeHandle> nh = subscriber->node_handle();
  std::string octomap_topic = "/octomap_full";
  ros::Publisher octomap_pub =
      nh->advertise<octomap_msgs::Octomap>(octomap_topic.c_str(), 1000);
  ROS_INFO("octomap_topic : %s", octomap_topic.c_str());

    //Uncomment below to publish distmap and vismap
    //ros::Publisher distmap_pub = nh->advertise<visualization_msgs::Marker >( "distmap_markers", 100 );
    //ros::Publisher vismap_pub = nh->advertise<visualization_msgs::Marker >( "vismap_markers", 100 );

    //ros::Publisher resetEDTPub = nh->advertise<std_msgs::Empty>("human_model/reset_edt", 1000);

    // ros::Subscriber resetEDTSub = nh->subscribe("human_model/reset_edt", 1000, &cb_resetEDT);

//   ros::ServiceServer srv_distToCollision = nh->advertiseService("/human_model/distance_to_collision", &cb_distToCollision);
    //ros::ServiceServer srv_updateDistMap = n.advertiseService("/human_model/update_dist_map", &cb_updateDistMap);

    // Publish octomap
    ros::Rate spin_rate(25);

  while (ros::ok()) {
    std::shared_ptr<const octomap::OcTree> octomap = subscriber->octomap();
    octomap_msgs::Octomap msg;
    octomap_msgs::binaryMapToMsg(*octomap, msg);
    std::cout << std::endl;
    msg.header.frame_id = subscriber->frame_id();

    if (!msg.data.empty()) {
        octomap_pub.publish(msg);

        // if (init_dist_map || resetEDT){
            
            
        //     if (init_dist_map){
        //         std::cout<<"Initializing EDT!"<<std::endl;
        //     }
        //     else if (resetEDT){
        //         std::cout<<"Resetting EDT!"<<std::endl;
        //     }
            
        //     //ros::Duration(1.0).sleep();
        //     bool unknownAsOccupied = false;
        //     double x,y,z;
        //     octomap->getMetricMin(x,y,z);
        //     octomap::point3d min(x,y,z);
        //     std::cout<<"metric min : "<<x<<" , "<<y << ", " << z << std::endl;
        //     octomap->getMetricMax(x,y,z);
        //     std::cout<<"metric max : "<<x<<" , "<<y << ", " << z << std::endl;
        //     octomap::point3d max(x,y,z);
        //     //- the first argument ist the max distance at which distance computations are clamped
        //     //- the second argument is the octomap
        //     //- arguments 3 and 4 can be used to restrict the distance map to a subarea
        //     //- argument 5 defines whether unknown space is treated as occupied or free
        //     //The constructor copies data but does not yet compute the distance map
        //     min.x()=-max_edt_dist;
        //     min.y()=-max_edt_dist;
        //     min.z()=-max_edt_dist;

        //     max.x()=max_edt_dist;
        //     max.y()=max_edt_dist;
        //     max.z()=max_edt_dist;

        //     // Note: these are expensive operations below and the max update rate I've gotten with them updating continuously is ~2Hz. This depends on the size of the map of course
        //     // This computes the distance map
        //     // std::lock_guard<std::mutex> lock(dist_map_mutex);
        //     dist_map_ptr.reset(new DynamicEDTOctomap (float(max_edt_dist), const_cast<octomap::OcTree*>(&*octomap.get()), min, max, unknownAsOccupied)); //const_cast<octomap::OcTree*>(&*octomap)
        //     dist_map_ptr.get()->update();
            
        //     octomap::point3d p(0.1775,0.9987,1.4101);
        //     std::cout<< dist_map_ptr.get()->getDistance(p) <<std::endl;
        //     init_dist_map = false;
        //     resetEDT = false;
        // }

         //Uncomment below to publish distmap and vismap
        // visualization_msgs::Marker EDTMarkers = makeEDTMarkers();
        //visualization_msgs::Marker visMarkers = makeVisibilityMarkers();
        //vismap_pub.publish(visMarkers);
        // distmap_pub.publish(EDTMarkers);
        
    }

    ros::spinOnce();    // Process callbacks.
    spin_rate.sleep();  // Sleep until next cycle.
  }
  subscriber->Stop();
  ros::waitForShutdown();
  return 0;
}
